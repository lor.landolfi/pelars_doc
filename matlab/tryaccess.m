mysetup('jsonmex')
mysetup('urlread2')

if ~exist('token','var')
    % need to user urlread2 due to buggy MATLAB
    [s,e] = urlread2(['http://pelars.sssup.it/pelars/password?' http_paramsToString({'user',auser,'pwd',apwd})],'POST','');
    if e.status.value == 200        
        token = getfield(fromjson(s),'token');
    else
        error('Wrong Login');
    end
end

[sa,e] = urlread('http://pelars.sssup.it/pelars/session','GET',{'token',token});
sa = fromjson(sa);

sessionid = 676;
[s,e] = urlread(sprintf('http://pelars.sssup.it/pelars/session/%d',sessionid),'GET',{'token',token});
s = fromjson(s);

