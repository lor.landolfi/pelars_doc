#Operations

Some basic operation have been implemented to extract new data from the stored sessions.\\
Two endpoints are dedicated to this task in the server:

* <http://pelars.sssup.it/pelars/op/[op_id]> 
* <http://pelars.sssup.it/pelars/result/{op_id}>

The first one is intended for submitting operation requests to the server while the second one to get results.
Operations are performed asynchronously: you submit an operation and then poll the server for the result instead of waiting for it. Further operations are to be discussed with the other PELARS partners.

At this very moment we designed and implemented the structure of the system and some basic operations.

Once a request is received by the server,  a message is immediately returned to the issuing client. This status message can be an error message if the request was ill formed or the notification that the server has correctly queued the request along with a unique identifier of the newly created job assigned by the system. A typical example of such a message is the following.


```json
{  
	"id":"53345",
	"status":"Submitted"
}
```


The type of job to be submitted is written in the field {\tt type} in the JSON body of the PUT issued on \url{/op}. 

The user can query the system any time to obtain the job status and in case its result. 
The </op/{job_id}> Rest interface will answer an HTTP GET operation with a status message. 

A job can be in one of the following four states which correspond to the status message: {\tt queued, executing, terminated, failed}. 
The answer to the GET operation on this endpoint will contain also a field called {\tt submission time} showing the date in which the job was enqueued for execution by the system.
Finally, if the status message is {\tt terminated}, the JSON answer will only contain the field {\tt execution time} indicating the time in milliseconds needed by the system to perform the job once popped from the queue. In the following, we show the structure of all the possible responses that can be obtained querying this endpoint via a GET.

```json
{  
	"execution time":34.382375,
	"status":"TERMINATED",
   "submission time":"2015/09/02 12:34:16"
}

{  
   "status":"EXECUTING",
   "submission time":"2015/09/02 12:34:16"
}

{  
   "status":"QUEUED",
   "submission time":"2015/09/02 12:34:16"
}

{  
	"status":"FAILED",
	"submission time":"2015/09/02 12:34:16"
}
```

We provide also the endpoint </op/result/{job_id}> supporting only GET requests that is in charge of returning the actual result of the job. If the job has terminated correctly and such a result is a single value result, the answer will contain only the field {\tt result} together with its value. 
In case the result is a stream, the answer will be a JSON array whose elements are the JSON representations of the stream result. 
Instead, if the operation has failed, the answer will contain a message field storing the reason of failure. Finally, if the status of the job is {\tt queued} or {\tt executing}, the answer will be a JSON object storing a "message" field stating that there are no results available yet.
In following paragraphs we show all the operations available right now. 

* To submit a job request to the server you must execute an HTTP PUT on <http://pelars.sssup.it/pelars/op>. The input of an operation is defined as the body of such a PUT.
*To get the status of a submitted job, you must perform an HTTP GET on <http://pelars.sssup.it/pelars/op/{op_id}>. Where op_id is the identifier of the job.
* When the output of the operation is a JSON array of Data samples, it is possible to filter it by time specifying the same parameters described in Section \ref{Data}.

In general, each operation can be performed on the portion of data samples captured during a particular phase specifying the optional JSON field "phase" in the JSON body of the PUT over <ttp://pelars.sssup.it/pelars/op>, hence such parameter is omitted in the following descriptions.
Also the PELARS session is a required integer input parameter, named "session".
An optional parameter that can be parsed in any operations input is "sync".
If "sync" is set equal to "true" then the operation is performed synchronously:  as soon as the result is ready or the operation fails the message is returned. Basically setting this parameter allows to merge the PUT on \url{http://pelars.sssup.it/pelars/op} with the GET on \url{http://pelars.sssup.it/pelars/op/{op\_id}}.

Other valuable endpoints allow to extract information about the operations by name or session identifier. Those are:

* <http://pelars.sssup.it/pelars/content/{session_id}/[{name}]>
* <http://pelars.sssup.it/pelars/content/{name}>

The first one returns the OpDetail JSON representations of all the operations computed over session "session_id" and having an optional custom name. The second one instead allows to query for the operations that have been executed on any session but identified by the specified name. Both of them return a JSON array that can be empty. 

Here we report a trivial example of how to query one of the previous endpoint with JavaScript.
```html
<html>
<head>
<script type="text/javascript" src="jquery/dist/jquery.js"></script>
<script>
$.getJSON("/pelars/content/1051/aftersession_hand_speed", function(handspeed) {
	perform(handspeed);
});

function perform(hs){
	
	var jres = hs[0]
	
	$("body").append(jres['result'])
}
</script>
</head>
</html>
```
##Mean value
###Input

* table: the table from which we want to extract the result
* field: field on which we want to compute the mean value

###Output
* result: the mean value of the extracted data

###Example
* input
```json
{  
	"type":"mean",
	"table":"Face",
	"field":"pos_x1",
	"session":"678"
}
```
* Output
```json
{  
	"result":128.873
}
```

##Variance
###Input
* same structure of mean

###Output
* result: the variance of the extracted data

###Example
* Input
```json
{  
	"type":"variance",
	"table":"Face",
	"field":"pos_x1",
	"session":"678"
}
```
* Output
```json
{  
	"result":55438.344
}
```

##Multistatistics
##Input
* same structure of mean and variance

##Output
* result: JSON object containing various statistical data. The meaning of each field of such a JSON object will be clear from the example

###Example
* Input
```json
{  
	"type":"multistatistics",
	"table":"Hand",
	"field":"pos_x0",
	"session":"678"
}
```
* Output
```json
{  
	"result":{  
		"min":1556.77001953125,
		"max":1601.300048828125,
		"mean":1584.289328342013,
		"variance":61118.641043647774,
		"stddev":247.22184580584252,
		"elements":135
   }
}
```

##Filter
###Input
* table: the table from which we want to extract the result
* expression: expression used to filter the result, can be any logical expression

###Output
* JSON array of data

###Example
* Input
```json
{  
	"type":"filter",
	"table":"Face",
	"expression":"time < 110.5 AND time > 44.89",
	"session":"189"
}
```

* Output
```json
[  
	{  
		"data_id":4668,
		"distance from camera": 1.13150625,
		"num":0,
		"pos_x1":317,
		"time":46.500732797,
		"pos_x0":297,
		"session":189,
		"pos_y0":127,
		"pos_y1":147,
		"type":"face"
	},
	{  
		"data_id":4669,
		"distance from camera": 1.00813433,
		"num":0,
		"pos_x1":247,
		"time":55.934821564,
		"pos_x0":227,
		"session":189,
		"pos_y0":7,
		"pos_y1":27,
		"type":"face"
	},
	{  
		"data_id":4670,
		"distance from camera": 0.63641222,
		"num":0,
		"pos_x1":433,
		"time":58.600992186,
		"pos_x0":413,
		"session":189,
		"pos_y0":8,
		"pos_y1":28,
		"type":"face"
	}
]
```

##Track
###Input
* table:  the table from which we want to extract the samples got by the system in the last minute

###Output
* JSON array of data

###Example
* Input
```json
{  
	"type":"track",
	"table":"Hand",
	"session":"189"
}
```

* Output
```json
[  
	{
		"data_id": 53266,
		"num": 784,
		"rw": 0.999999,
		"rx": 3.03634E-4,
		"ry": -0.00108022,
		"rz": -6.08182E-5,
		"session": 1266,
		"time": 2841,
		"tx": 1.70484E-4,
		"ty": 1.85072E-4,
		"type": "hand",
		"tz": 6.57141E-8
	},
	{
		"data_id": 53267,
		"num": 960,
		"rw": 0.999381,
		"rx": 3.03188E-4,
		"ry": 6.66941E-4,
		"rz": 0.0351745,
		"session": 1266,
		"time": 2841,
		"tx": 0.279248,
		"ty": -0.115026,
		"type": "hand",
		"tz": 6.05506E-6
	}
]
```

##Frequency
This operation counts the number of samples collected during a session or a phase

###Input
* nothing optional

###Output
* number: number of counted samples
* frequency: number of found items divided by the time interval in seconds

###Example
* Input
```json
{
    "type" : "frequency",
    "table" : "Ide",
    "session" : 1320
}
```

* Output
```json
{
  "result": {
    "number": 222,
    "unit": "#/s",
    "frequency": 0.05814963157493562
  }
}
```

##Media info
###Input
* nothing optional

###Output
* result: JSON object containing various information: the number of submitted words per multimedia entity, the total number of words submitted, the total number of posts, the number of videos and images submitted.

###Example
* Input
```json
 {
    "type" : "media_info",
    "session" : 542,
    "phase" : "plan"
}
```

* Output
```json
{
  "result": [
    {
      "id": 603,
      "words": 19
    },
    {
      "total_words": 19
    },
    {
      "videos": 0
    },
    {
      "images": 1
    },
    {
      "total_posts": 2
    }
  ]
}
```

##Time looking

###Input
* nothing additional

###Output
* result: JSON object containing two fields: active time and inactive time, indicating respectively the overall estimated time spent looking at the screen and not looking at it. Both the time values are expressed as percentage of the session duration.

###Example
* Input
```json
{
    "type" : "time_looking",
    "session" : 1371
}
```

* Output
```json
{
  "result": {
    "inactive_time": 77.88469693260173,
    "active_time": 22.115303067398273
  }
}
```

##Hand speed

###Input
* nothing additional

###Output
* result:  JSON array containing the average velocity of each tracked hand and the overall average speed, in meters per second.

###Example
* Input
```json
{
    "type" : "hand_speed",
    "session" : 542
}
```
* Output
```json
{
  "result": [
    {
      "num": 64,
      "speed": 0.10071244525027565
    },
    {
      "num": 320,
      "speed": 0.061795318785768334
    },
    {
      "num": 336,
      "speed": 0.0627333174911541
    },
    {
      "overall": 0.07508036050906602
    }
  ]
}
```

##Number of Faces
This operation returns the approximated number of faces looking at the screen at a given time. Since we may not know exactly how many faces are looking at the screen at a given moment, the result is obtained by linearly interpolating our data.

###Input
* time: moment when we want to calculate the number of faces looking at the screen. (Seconds elapsed from the beginning of the session).

###Output
* result: number of faces

###Example
* Input
```json
 {
    "type" : "num_faces",
    "session" : "542",
    "time" : 13.5
}
```

* Output
```json
{
  "result": "1.0"
}
```

##Presence
This operation is used to estimate the presence at the desk of any student during a specific session or session phase. The portion of session time taken in consideration is split into intervals (currently of fixed size: 1.1 seconds); for each interval we decide that there is a presence at the desk if the distance from the camera of at least one face sample recorded in that time is less than 1.7 meters or there is at least one hand sample less than 1.3 meters distant from the origin marker.

###Input
* details: boolean, if set to "true", the output of the operation will include the estimated presence for each timestep. If set to "false", only the total absence and presence time will be reported.

###Output
* result: JSON object composed by the following.
* presences: JSON array containing of JSON objects. Each object indicates the start and end of each time interval taken in consideration (in UNIX epoch milliseconds) and a boolean stating whether the presence has been estimated or not in that interval of time  
* total_absence: estimated time of obsence from the table during the interval of time taken into account, as percentage of session duration
* total_presence: estimated time of presence at the table during the interval taken into account, as percentage of session duration

###Example
* Input
```json
{
    "type" : "presence",
    "session" : "665",
    "details" : true
}
```

* Output
```json
{
  "result": {
    "presences": [
      {
        "start": 1464080356064,
        "end": 1464080357164,
        "presence": true
      },
      {
        "start": 1464080357164,
        "end": 1464080358264,
        "presence": true
      },
      {
        "start": 1464080358264,
        "end": 1464080359364,
        "presence": true
      },
      {
        "start": 1464080359364,
        "end": 1464080360464,
        "presence": false
      },
      {
        "start": 1464080360464,
        "end": 1464080361564,
        "presence": true
      },
      {
        "start": 1464080361564,
        "end": 1464080362664,
        "presence": true
      },
      {
        "start": 1464080362664,
        "end": 1464080363764,
        "presence": false
      },
      ...
     ],
    "total_absence": 35.84952055733443,
    "total_presence": 63.63143215127527
}
```

##Programming time
This operation estimates the amount of time dedicated to programming

###Input
* no additional fields

###Output
* programming_time: amount of programming time as a percentage of the interval of time taken into account. Can be a phase or the whole session.

###Example
* Input
```json
{
    "type" : "programming_time",
    "session" : "1320"
}
```

* Output
```json
{
  "result": {
    "programming_time": 60.12509504976378,
    "unit": "%"
  }
}
```

##Video from snapshots
This operation creates a video from a sequence of snapshots captured during session progress.

###Input
* input_framerate: input frame rate
* output_framerate: output frame rate
* view: identifies perspective of snapshots. It can be "workspace":snapshots from kinect, "people":snapshots from camera, "screen":screenshots. Default: "workspace".

###Output
* result: the URL of the generated video

###Example
* Input
```json
{
    "type" : "video_snapshots",
    "session" : 1159,
    "input_framerate" : 1,
    "output_framerate" : 30,
    "view" : "workspace"
}
```

* Output
```json
{
  "result": "http://pelars.sssup.it/pelars/multimedia/1159/5743"
}
```
The generated video has each snapshot lasting for one second, with framerate of 30 frame per second.

##Proximity
This operation calculates the distance (in meters) between all possible pair of hands or faces sampled at the same instant of time. It may output all the distance values in a JSON array or just the statistics extracted by them.

###Input
* table: only "Hand" or "Face" are accepted 
* statistics: outputs only statistics extracted from computation rather than all the distances

###Output
* result: a JSON object containing statistic values or a JSON array of distance and timestamps

###Example
* Input
```json
{
    "type" : "proximity",
    "session" : 1371,
    "table" : "Hand",
    "statistics" : true
}
```

* Output
```json
{
  "result": {
    "min": 0.22961587372643474,
    "max": 1.0927457878235582,
    "variance": 0.0012666711833210099,
    "mean": 0.49442612081053866,
    "elements": 121,
    "stddev": 0.03559032429356341
  }
}
```

* Input
```json
{
    "type" : "proximity",
    "session" : 1371,
    "table" : "Face"
}
```

* Output
```json
{
  "result": [
    {
      "distance": 0.46586541416337807,
      "num1": 0,
      "angle": 16.373330063606073,
      "time": 1464080368827,
      "num2": 1
    },
    {
      "distance": 0.9161896887266121,
      "num1": 0,
      "angle": 165.3023995991482,
      "time": 1464080371876,
      "num2": 1
    },
    {
      "distance": 0.5078139045472863,
      "num1": 0,
      "angle": 90.73171826365889,
      "time": 1464080448002,
      "num2": 1
    },
    ...
    ]
}
```
Fields "num1" and "num2" are the identifiers of the two face or hand sample taken into account for distance calculation. They correspond to the "num" field of the two hand or face samples on which distance is computed. 

##Object detector
This operation has the aim to track the center of an object in a video obtained from the snapshots generated during a PELARS session.

###Input
* start: index of the first snapshot used to track the objects
* filter_length: size of the filter that averages data
* boxes: JSONArray of JSONObjects representing the squares to be tracked. Objects are described by the x and y pixel coordinates of the upper left corner, by the width and height of the square and by a name.
* video: optional, if it is true an output video is generated, otherwise not. (Default true).
* video_id: optional, in case it is specified the operation tracks the video identified by this parameter instead of generating a new one.

###Output
* url: the URL of the generated video (where tracking is visible)
* boxes: JSONArray of JSONObjects, each element of the array is described by the following fields:
* data: JSONArray, each entry is the representation of the tracked object ("name" and "x,y" coordinates)
* time_stamp: timestamp of the snapshot over which the algorithm has been computed

###Examples
* Input
```json
{
    "type" : "object_detect",
    "session" : 1051,
    "start" : 3,
    "filter_lenght" : 6,
    "boxes" : [
    {
    "x" : 1119,
    "y" : 310,
    "width" : 141,
    "height" : 103,
    "name" : "scotch"
    }
    ]
}
```

* Output
```json
{
  "result": {
    "boxes": [
      {
        "data": [
          {
            "name": "scotch",
            "x": 733.612,
            "y": 569.433
          }
        ],
        "time_stamp": 1455634438988,
        "frame": 0
      },
      {
        "data": [
          {
            "name": "scotch",
            "x": 735.471,
            "y": 566.245
          }
        ],
        "time_stamp": 1455634499090,
        "frame": 1
      },
      {
        "data": [
          {
            "name": "scotch",
            "x": 739.846,
            "y": 570.588
          }
        ],
        "time_stamp": 1455634559241,
        "frame": 2
      },
      {
        "data": [
          {
            "name": "scotch",
            "x": 721.609,
            "y": 650.808
          }
        ],
        "time_stamp": 1455634619268,
        "frame": 3
      },
      ...
    ],
   "url": "http://pelars.sssup.it/pelars/multimedia/1051/6883"
	}
}
```
A useful and and human friendly web interface for invoking this operation is located at [http://pelars.sssup.it/pelars/object_selector.html?session=1051](http://pelars.sssup.it/pelars/object_selector.html?session=1051).

##Merge
This operation can be used to merge two sessions. It is useful if for instance a session crashed and another one is issued after a while which is actually the second part of the crashed one.
Merging is performed in the following way.\\
All the data belonging to the session opened later are shifted in time by the difference between the start of the later session with the end of the earlier session. "setup" phase of the later session is removed and the end of the last phase of the earlier session is set to the start of the first phase following the "setup" one in the later phase.
This operation copies all the data belonging to both the session and the result of the merging is a \textbf{new} session.

###Input

* with: session to be merged with

###Output
* result: the identifier of the brand new merged session 

###Example
* Input
```json
{
    "type" : "merge",
    "session" : 1595,
    "with" : 1596
}
```
* Output
```json
{
    "result" : 1640
}
```
In order to provide some actual examples of merged sessions, in the PELARS dataset session 1602 is the result of the merging of session 1592 with 1594, session 1600 is the merge between 1587 and 1591, session 1599 is the merge between session 1586,1589,1590. 

##Pipeline

###Input
* operations: list of operations to be executed one after the other

###Output
* result: the output of the last operation of the pipeline

###Example
* Input
```json
{
    "type" : "pipeline",
    "session" : "542",
    "operations" : 
        [
            {
                "type" : "filter" ,
                "table" : "Face" ,
                "expression" : "distance > 1.0 AND distance < 1.4",
                "session" : "542"
            },
            {
                "type" : "mean",
                "field" : "distance",
            }
        ]
}
```	
* Output
```json
{
  "result": "1.10210271298777"
}
```

##Map

###Input
* operation: operation to be executed several times over a split input
* window_size: size of a single time window on which the operation is performed
* overlap: percentage representing how much the windows overlap with each other
For now the input of the operation to be executed must be a sequence of data samples and the split is only in time.

###Output
* result: the outputs of the operation performed on the windowed input. For each output the starting and ending time of the window are also recorded as values of the JSON fields "start" and "end". 

###Example
* Input
```json
{
    "type" : "map",
    "session" : "542",
    "operation" : {
            "type"  : "multistatistics",
            "table" : "Face",
            "field" : "distance",
            },
    "window_size" : 550000,
    "overlap" : 10,
    "parallelism" = 4,
    "name" = "important"
}
```

* Output
```json
{
  "result": [
    {
      "min": 0.32190789473684206,
      "max": 4.893,
      "start": 1448014495401,
      "thread": 565,
      "mean": 1.5170164259021337,
      "variance": 1.2682355630805948,
      "stddev": 1.1261596525717812,
      "elements": 154,
      "end": 1448015045401
    },
    {
      "min": 0.29387387387387387,
      "max": 4.893,
      "start": 1448014990401,
      "thread": 566,
      "mean": 0.924897293406578,
      "variance": 0.004087889547319286,
      "stddev": 0.06393660569125707,
      "elements": 580,
      "end": 1448015540401
    },
    {
      "min": 0.5233155080213904,
      "max": 4.893,
      "start": 1448015485401,
      "thread": 567,
      "mean": 0.927957761712495,
      "variance": 0.007133973918329228,
      "stddev": 0.08446285525797259,
      "elements": 543,
      "end": 1448016035401
    },
    {
      "min": 0.5233155080213904,
      "max": 4.893,
      "start": 1448015980401,
      "thread": 568,
      "mean": 1.0109478797306148,
      "variance": 0.03972677967472672,
      "stddev": 0.1993157787901568,
      "elements": 454,
      "end": 1448016530401
    },
    {
      "min": 0.9985714285714286,
      "max": 4.893,
      "start": 1448016475401,
      "thread": 568,
      "mean": 2.1611463874154015,
      "variance": 4.942734076080435,
      "stddev": 2.2232260515027336,
      "elements": 10,
      "end": 1448017025401
    }
  ]
}
```

##Group
The aim of this operation is to pump some operations in the system with a single http PUT. Each operation is given an unique id, as it is invoked apart.

###Input
* operations: JSON array whose elements are the inputs of the operations to be run

###Output
* empty, the outputs of the operations must be retrieved from their specific ids or name

###Example
* Input
```json
{
    "type" : "group",
    "session" : "1371",
    "name" : "after session routine",
    "operations" : 
        [
            {
                "type" : "hand_speed" ,
                "session" : "1371",
                "name" : "g1_hand_speed"
            },
            {
                "type" : "time_looking",
                "session" : 1371,
                "name" : "g1_time_looking"
            },
            {
                "type" : "media_info",
                "session" : 1371,
                "name" : "g1_media_info"
            },
            {
                "type" : "proximity",
                "session" : 1371,
                "table" : "Hand",
                "statistics" : true,
                "name" : "g1_proximity_hand"
            },
            {
                "type" : "proximity",
                "session" : 1371,
                "table" : "Face",
                "statistics" : true,
                "name" : "g1_proximity_face"
            }
        ]   
}
```

##A simple tool to test our REST API
Anyone can test our REST api easily by using https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop (POSTMAN), a Google Chrome App. User credentials are: pelarsteam pelarsteam.

##Important notice
We have already pre-computed some relevant operations over the valid sessions having identifier grater than 1000 (valid sessions are the ones that can be obtained via [http://pelars.sssup.it/pelars/goodsession](http://pelars.sssup.it/pelars/goodesession)) Details of each operation can be queried using operations names. Basically, for each valid session a "Group" operation has been launched with the following input.

```json
{
    "type" : "group",
    "session" : "session_id",
    "name" : "after session routine",
    "operations" : 
        [
            {
                "type" : "hand_speed" ,
                "name" : "aftersession_hand_speed"
            },
            {
                "type" : "time_looking",
                "name" : "aftersession_time_looking"
            },
            {
                "type" : "media_info",
                "name" : "aftersession_media_info"
            },
            {
                "type" : "proximity",
                "table" : "Hand",
                "statistics" : true,
                "name" : "aftersession_hand_proximity"
            },
            {
                "type" : "proximity",
                "table" : "Face",
                "statistics" : true,
                "name" : "aftersession_face_proximity"
            },
            {
                "type" : "presence",
                "name" : "aftersession_presence"
            }
        ]   
}
```