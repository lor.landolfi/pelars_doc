#HowTo

This section explains the common operations which can be performed with the Pelars client.

#Preliminary steps

After the complete installation of the tool there are three fundamental steps which have to be performed befor using the tool

1) Add you login credentials and the data you want to add to the session in the personal.xml file located in the data folder of the pelars folder. If you have no login credentials the you have to register as explained in the data access section.

2) Insert in the sse.txt file the access credentials for the particle.io devices you want to use. If you have no intention to use them you can leave the file as it is

3) Calibrate the cameras of the pelars tool. To do this print the Aruco marker with id 0, which can be done using the provided tool or the precreated file in the pelars marker folder,  and put it on the table. Execute the pelars tool with the -c flag and move the marker around until you can see 3d axis in both images on the marker. Turn the marker in such a way that the y axis is point outwards in respect to the cameras. Once both cameras have 3d axis on the marker press the c button. Calibration is now terminated.

#Execution Flags

The first parameter is the flag which can be added with -- and the second charachter is the shortcut which can be added with -. 
ex: --face or -f 

- help : "displays a help message"
- face,f : "track the faces"
- mongoose,m : "mongoose port for the arduino ide"
- audio,a : "track audio level"
- marker,M :  float, "marker size"
- hand,h : "track the hands"
- particle,p : "track the particle IO sensors"
- ide,i : "track the Arduino IDE log"
- visualization,v : "activate visualization"
- qr,q : "show session as qr code"
- server,S : string, "server endpoint"
- session,x : int, "session id for the upload tool"
- face_camera,F : int, "video device id (ex: for /dev/video3 use -f 3)"
- calibration,c : "calibrate cameras"
- special,s : "special flag for background run"
- processor,X : int, "kinect2 processor : 0 for CPU, 1 for OPENGL, 2 for OPENGL. default 1"
- upload,u : string, "file name to upload to the server"
- status,j : "displays system status"
- test,t : "create a test session"
- default,D : "create a session with default flags (-f -h -a -i -j -q)"
- video,V : "records kinect and webcam video streams as h264 videos"

A normal execution could be for example ./pelars.sh -D

#Upload

An offline (or partially) session can be uplaoded to the server using the -u flag of the collector. There are three different modalities in which the collector can work:

- Offline : If the user did not get any session ide, identified as session -1, the user can upload the session using the -u flag folowed by the file containing the backup which is named using the session id (-1), the date. This will automatically create a new session on the server and pload all the data to it.

ex: ./pelars.sh -u backupfile

- partially offline : If the user got a valid session id but at some point the connection went into an offline state then the user can upload the data using the -u flag followed the the backup file name containing the session id. This will resend all data to the server, which will just add the missing data, skipping the data which is already present.

ex: ./pelars.sh -u backupfile

- Client offline, mobile online : If there is no connection on the client, it is possible to open a new session from the mobile tool and collect data offline as usual from the client. At the end all the data can be uploaded the the session opened by the mobile tool by using the -u flag followed by the backup file with session (-1) and by the -x flag followed by the session identifier.

ex: ./pelars.sh -u backupfile -x session_id

The backup file contains the word local or backup. The first one means that the session was not closed correctly and so some data might be missing. The second term means that all data has been uploaded during the sssion successfully and no further upload should be necessary. 
Since the data amount could be quite big and the server need to process all the sent data, data is sent in chunks, 300 packets at a time. This might take some minutes to upload all the data.

#System status

To check live the sattus of a Pelars session you can use the endpoint http://pelars.sssup.it/pelars/status.jsp?session={id} where id is the session id. This endpoint will give you the online/offline status.