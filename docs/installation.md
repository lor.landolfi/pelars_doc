#Installation

This section will explain in details how to install the pelars system

#Preliminary step:

Register on www.pelars.sssup.it/pelars. Please contact me at giacomo.dabisias@gmail.com in order to authorize your registration
request.
	
#Ubuntu installation

Download ubuntu 14.04 iso and create a bootable disk (either dvd or usb stick).
Start the installation process by booting from the selected device. Follow the instructions to install Ubuntu.

If you have issues installing the operating system like getting a black screen after pressing install then do the following:

1) when requested to choose between install ubuntu and try ubuntu press 'e' on install. Edit the opened file putting the following string with NO quotes after the splash word

"nomodeset vga=791"

2) Press f10 or crtl+x to proceed with the installation.

Don't install any video card drivers. The installation script will install the drivers.

#Pelars installation

Open a terminal on the new system and download the pelars client from the bitbucket repository <https://bitbucket.org/pelars_sssa/pelars_clientdeploy>.
From a terminal move into the docker folder inside the pelars folder on the desktop and execute ./init.sh. This will reboot the system.
After that open again the terminal and move again to the same folder and execute ./prepare.sh. This will work for some time with a black screen. Don't worry, eventually it will reboot.
After rebooting you system should be ready.

Known issues

* Be sure that google dns 8.8.8.8 can be used in your network. If you can't open the docker configuration fiel and add your dns
* If you cant get the ubuntu graub start screen on boot hold "crtl" or "spacebar" to have it appear and be able to modify the options as stated
in point 2)

#Example
example to add 172.18.20.13
```
example@ubuntu ~ $ sudo nano /etc/default/docker
...
...
# Use DOCKER_OPTS to modify the daemon startup options.
DOCKER_OPTS="--dns 172.18.20.13"
...

```

#Marker creation

To create markers you can use the marker.py script. To use it just execute python marker.py --pages 2 --first 1 --markersize 35 --bordersize 5 --border --rows 6 --pagemargin 0 --cols 4 .
There are already two pages with markers in pdf in the marker folder. Don't use the first marker (one white line) since it has a special calibration use. With pages you can specify the number of pages of markers that you want.
There is also a script to create the 0 marker which is bigger as the usual markers.