

# Documenting the Content

B1 - block created, <br />
B2 - block removed,<br />
M1 - module connected,<br />
M2 - module disconnected,<br />
L1 - link created,<br />
L2 - link removed,<br />
CC - config changed,<br />
BP - button pressed<br />

* hardware block: [M,blockid genericname]
* software block: [B,blockid genericname]
* link: [L, blockid name blockid2 name outletid inletid]
* button press: BP#
* settings change: [CC, blockid value]