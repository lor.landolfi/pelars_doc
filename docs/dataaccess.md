
#Account 

In order to get access to the services available on the server, any user has to be in possession of an active account. It is possible to create a new account by clicking on the subscribe button at <http://pelars.sssup.it/pelars/>.

You must fill in the form with the requested data and then click the submit button. If the data is correct, a success message will appear and you will be redirected to the login page. The newly created account is in PENDING state and the server administrator will validate the provided information making it ACTIVE. At the moment SSSA administers the server so the account validation time is not immediate

After validation, if you submit the same email and password you submitted in the registration page, you will be granted access to the REST operations exposed by the server. 

#Web Browser
If you want to access the server resources using a web browser, then you have to go to the login page <http://pelars.sssup.it/pelars> and submit your registered data. If the login is successful the server resources will get granted to the web resources by entering the URL. The login is kept across sections thanks to a cookie. 
First version of entry dashboard is provided at <http://pelars.sssup.it/pelars/welcome-page.jsp>, allowing you to edit and view the data of your personal sessions, along with some of your personal details.

#External Program
If you are developing a program or a servlet that has to interact with the PELARS server the authentication is based on an access token, store it as a variable, and then pass it to any request you are going to perform on your program as a standard {\tt HTTP} parameter named {\tt token}. \\
A very simple example of a javascript/html client can be the one described in the following.\\


```html
<html>
<script type="text/javascript"
src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type = "text/javascript" src = "http://pelars.sssup.it/pelars/auth.js"></script>
<script type="text/javascript">
	$(document).ready(function() { 
	var token = pelars_authenticate();
		$.getJSON(
		"http://pelars.sssup.it/pelars/session?token="+token,
		function(json) {
		document.getElementById("id").innerHTML = JSON.stringify(json);
		});
	});
</script>
<body>
<p id = "id"> </p>
</body>
</html>
```


In order to access the resources on the server you have to use the javascript script located at <http://pelars.sssup.it/pelars/auth.js>. 
This file contains the implementation of the function called ``pelars_authenticate``.
It asks the user for the PELARS credentials and in case of success, it returns the access token that allow you to access the server resources, else it returns 0. The behaviour of the illustrated html page can be summarized in the following way. 
As soon as the document is loaded ``$(document).ready``, you call the ``pelars_authenticate()`` function and you store the result in the variable token. After this, you perform an automated GET rest request on the {\tt /session} endpoint appending the token as an HTTP parameter. 

You can also specify your username and password as parameters of the ``pelars_authenticate()``. In this way you do not have to fill the form every time. Hence the script above would become the following. 

```html
<script type="text/javascript">
	$(document).ready(function() { 
	var token = pelars_authenticate("youremail", "yourpassword");
		$.getJSON(
		"http://pelars.sssup.it/pelars/session?token="+token,
		function(json) {
		document.getElementById("id").innerHTML = JSON.stringify(json);
		});
	});
</script>
```

##Python Example

Assuming to have imported json and requests packages:

```python
def gettoken(user,password):
	session = requests.Session()
	r = session.post('http://pelars.sssup.it/pelars/password',params={"user":user,"pwd":password})
	if r.status_code != 200:
		return None
	else:
		return json.loads(r.text)["token"]
```

It is also possible to access and to test all the REST API exposed by the PELARS interface using POSTMAN: a chrome app. POSTMAN requires user authentication, type "pelarsteam" as username and "pelarsteam" as password. In order to not put the token in every POSTMAN query you may also install POSTMAN interceptor, which is another chrome plugin. 

