#Face

Since a face can be approximated to a rectangle, we sample the two points of the diagonal of the rectangle identifying the face. The coordinates reported on the data representation are the cartesian coordinates of the pixels associated to those points. 
In the future they will be the 3D coordinates in space, having the origin of the axis fixed to a certain point on the table. In the following we illustrate the name of the fields of the JSON object representing a face.


* pos_x0: abscissa of the leftmost lower pixel of the diagonal of the rectangle approximating the face
* pos_x1: abscissa of the rightmost higher pixel of the diagonal of the rectangle approximating the face
* pos_y0: ordinate of the leftmost lower pixel of the diagonal of the rectangle approximating the face
* pos_y1: ordinate of the rightmost higher pixel of the diagonal of the rectangle approximating the face
* distance: the approximated distance from camera in meters. 


#example

```json
{  
	"data_id":81145,
	"distance": 1.13150625,
	"num":0,
	"pos_x0":116,
	"pos_x1":235,
	"pos_y0":47,
	"pos_y1":166,
	"session":770,
	"time":1.445941734416E12,
	"type":"face"
}


```