#Hands

Hands are identified by the collector thanks to some square markers put on the wrist of the users. Each marker has a unique predefined "id" and we get the position of an hand looking for the center of such markers. 

We get the 3D coordinates of such markers setting the origin of the axes in a point located on the table. This point is actually identified by a special marker with "id" equal to zero.

Hence the characteristics of an hand sample are described by the following list:

* time and date
* data_id is the key
* session
* num: is the marker identifier
* tx,ty,tz: translation
* rx,ry,rz,rw: rotation quaternion
* type: "hand"

The coordinate system is the Table origin.

#Example

```json
{
        "data_id": 89115,
        "num": 64,
        "rw": 0.799751,
        "rx": 0.350779,
        "ry": 0.014286,
        "rz": -0.486979,
        "session": 542,
        "time": 1.448014524663E12,
        "tx": -0.1016,
        "ty": -0.2726,
        "type": "hand",
        "tz": -4.08592E-4
}
```


#Command Line Processing

The jq tool can be used for processing the JSON data. For example the following takes data and filters out hands

```json
jq '[.[] | select(.type == "hand")]' data615.json 
```

The following instead builds positions in 3D and hands identifiers all as numbers

jq '[.[] | select(.type == "hand") | [.num,.tx,.ty,.tz]]' data615.json