#Multimedia

The URL that follows is used to obtain information about the multimedia sent during a pelars session. 
```url
http://pelars.sssup.it/pelars/multimedia/{session_id}/[{multimedia_id}]/[{extra}]
```
If {multimedia_id} is not specified we get the representation of all the multimedia posted during the session identified by session_id, else we get the actual content of the multimedia identified by multimedia_id, which can be video, image or text.
In case the path parameter {extra} is equal to "thumb", a thumbnail of the image is returned instead of the whole one.\\
Else, if {extra} is equal to "meta", then the JSON representation if the entity is returned.
The generic multimedia entity is made by the following fields.

```url
http://pelars.sssup.it/pelars/multimedia/{session_id}/[{x}]
```

The url above instead is used to retrieve all the multimedia content descriptors having type specified by the parameter. {x} can be for instance "image" or "text".
Parameter x can also be considered as value of the "view" field of the multimedia (mobile, workspace, people, screen).

* data: the url on which is the actual content of this multimedia is located
* id: identifier of the multimedia entity
* mimetype: the mimetype of the multimedia (e.g. text, image, video)
* type: the type of the multimedia (e.g. plain, png, mp4)
* time: time at which the multimedia content has been posted to the server. Such time is in unix epoch milliseconds\footnote{Number of milliseconds that have elapsed since January 1, 1970 (midnight UTC/GMT)}.
* session: identifier of the pelars session this multimedia is associated to
* triggering: identifies if the triggering of the image is manual by buttons or automatic (once every minute)
* creator: identifies if the content is created by students or teachers or researchers
* view: identifies the perspective or the source that created this multimedia: "people"= multimedia originated from webcam, "workspace"= originated by the kinect, "screen"=screenshots, "mobile"= originated by mobile app. 

Three different images are captured when a button is pressed by the collector; one of the working area, one of the sorrounding and a screenshot of the ide. The remaining multimedia content is captured with the mobile application, but any multimedia content can be uploaded with a put request.

#example

- Request:  <http://pelars.sssup.it/pelars/multimedia/542>
- Response: 

```json
{
    "data": "http://pelars.sssup.it/pelars/multimedia/1186/2893",
    "id": 2893,
    "mimetype": "jpg",
    "session": 1186,
    "size": 748300,
    "time": 1.459331462456E12,
    "trigger": "automatic",
    "type": "image",
    "view": "workspace"
},
{
    "data": "http://pelars.sssup.it/pelars/multimedia/1186/2894",
    "id": 2894,
    "mimetype": "jpg",
    "session": 1186,
    "size": 107548,
    "time": 1.459331462769E12,
    "trigger": "automatic",
    "type": "image",
    "view": "people"
},
{
    "data": "http://pelars.sssup.it/pelars/multimedia/1186/2895",
    "id": 2895,
    "mimetype": "png",
    "session": 1186,
    "size": 1022812,
    "time": 1.459331463222E12,
    "trigger": "automatic",
    "type": "image",
    "view": "screen"
},
{
    "creator": "student",
    "data": "http://pelars.sssup.it/pelars/multimedia/1186/2896",
    "id": 2896,
    "mimetype": "plain",
    "session": 1186,
    "size": 36,
    "time": 1.459331504219E12,
    "type": "text",
    "view": "mobile"
}
```

- Request:  <http://pelars.sssup.it/pelars/multimedia/542/604>
- Response: [image](report/604.jpg)

- Request:  <http://pelars.sssup.it/pelars/multimedia/1051/2368/meta>
- Response: 

```json
{
	"view":"people",
	"size":140540,
	"data":"http://pelars.sssup.it/pelars/multimedia/1051/2368",
	"session":1051,
	"mimetype":"jpg",
	"id":2368,
	"time":1.455638159994E12,
	"type":"image"
}
```

##Complete session videos
The following url is used to get the complete videos of the sessions, both from kinect and webcam.
```url
http://pelars.sssup.it/pelars/completevideo/{session_id}/{webcam||kinect}
```
the part following the last slash must be either "kinect" or "webcam".