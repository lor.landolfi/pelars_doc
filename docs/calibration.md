#Calibration

A calibration represents a 4x4 matrix composed by a rotation matrix and a tanslation vector relative to the calibrtion marker.
The rotation matrix R is a 3x3 matrix and the translation T vector is a 3x1 vector. They are composed in a rototranslation as follows:


| R  T |                                   
| 0  1 |

The calibration endpoint is www.pelars.sssup.it/pelars/calibration/{id} where id is the session id.
The following fields are contained in the calibration object:

* parameters: a 12 element vector which represents the matrix rowmajor
* session
* type: device identifier

Only GET operations are allowed on this endpoint 

#Example

```json
{
	"parameters": [
	    0.06005922704935074,
	    -0.9981419444084167,
	    0.01027580630034208,
	    0.08054405450820923,
	    -0.1948215514421463,
	    -0.02181778475642204,
	    -0.9805960059165955,
	    0.3755728006362915,
	    0.9789981842041016,
	    0.05689188838005066,
	    -0.1957699209451675,
	    1.306175708770752
	],
	"session": 1042,
	"type": "webcam"
    
```