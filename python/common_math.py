import numpy,cv2,math



def quaternion_matrix(quaternion):
    """Return homogeneous rotation matrix from quaternion.

    >>> M = quaternion_matrix([0.99810947, 0.06146124, 0, 0])
    >>> numpy.allclose(M, rotation_matrix(0.123, [1, 0, 0]))
    True
    >>> M = quaternion_matrix([1, 0, 0, 0])
    >>> numpy.allclose(M, numpy.identity(4))
    True
    >>> M = quaternion_matrix([0, 1, 0, 0])
    >>> numpy.allclose(M, numpy.diag([1, -1, -1, 1]))
    True

    """
    q = numpy.array(quaternion, dtype=numpy.float64, copy=True)
    n = numpy.dot(q, q)
    if n < _EPS:
        return numpy.identity(4)
    q *= math.sqrt(2.0 / n)
    q = numpy.outer(q, q)
    return numpy.array([
        [1.0-q[2, 2]-q[3, 3],     q[1, 2]-q[3, 0],     q[1, 3]+q[2, 0], 0.0],
        [    q[1, 2]+q[3, 0], 1.0-q[1, 1]-q[3, 3],     q[2, 3]-q[1, 0], 0.0],
        [    q[1, 3]-q[2, 0],     q[2, 3]+q[1, 0], 1.0-q[1, 1]-q[2, 2], 0.0],
        [                0.0,                 0.0,                 0.0, 1.0]])

def arucostdpoints(ms):
    hs = ms/2
    return [(-hs,-hs,0,1),(-hs,hs,0,1),(hs,hs,0,1),(hs,-hs,0,1)]

def makeint(x):
    return tuple([int(y) for y in x])


# origin,x,y
def makepose_from_3_points(pp):
    p0 = np.array(pp[0])[0:3];
    px = np.array(pp[1])[0:3];
    py = np.array(pp[2])[0:3];
    x = (px-p0)
    x = x / np.linalg.norm(x)
    y = (py-p0)
    y = y / np.linalg.norm(y)
    z = np.cross(x,y)
    y = np.cross(z,x)

    r = np.identity(4)
    r[0:3,0] = x
    r[0:3,1] = y
    r[0:3,2] = z
    r[0:3,3] = p0
    return r