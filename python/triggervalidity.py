import pelarslas,argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Post Session')
	parser.add_argument('sessions',type=int,nargs="*")

	pelarslas.extendargs(parser)
	args = parser.parse_args()
	c = pelarslas.autoconnect(args)

	for session in args.sessions:
		print c.setvalid(session)
