from requests import Session
import json,os,pprint

# HEAD requests ask for *just* the headers, which is all you need to grab the
# session cookie
"""
response = session.post(
    url='http://sportsbeta.ladbrokes.com/view/EventDetailPageComponentController',
    data={
        'N': '4294966750',
        'form-trigger': 'moreId',
        'moreId': '156#327',
        'pageType': 'EventClass'
    },
    headers={
        'Referer': 'http://sportsbeta.ladbrokes.com/football'
    }
)
"""

def getsessions():
    session = Session()
    r = session.get('http://pelars.sssup.it/pelars/session',params={"token":os.environ["PELARSTOKEN"]})
    if r.status_code != 200:
        return None
    else:
        return json.loads(r.text)

def getsession(sessionid):
    session = Session()
    r = session.get('http://pelars.sssup.it/pelars/session/%d' % sessionid,params={"token":os.environ["PELARSTOKEN"]})
    if r.status_code != 200:
        return None
    else:
        print r.text
        return json.loads(r.text)

def gettoken(user,password):
	session = Session()
	r = session.post('http://pelars.sssup.it/pelars/password',params={"user":user,"pwd":password})
	if r.status_code != 200:
		return None
	else:
		return json.loads(r.text)["token"]


if __name__ == '__main__':
    if "PELARSTOKEN" in os.environ:
    	token = os.environ["PELARSTOKEN"]
    else:
        # TODO store somewhere
        import getpass
    	token = gettoken("giacomo.dabisias@gmail.com",getpass.getpass("User password"))
    	print "new token",token
        os.environ["PELARSTOKEN"] = token
        # TODO store somewhere

#print getsessions()
pprint.pprint(getsession(676))