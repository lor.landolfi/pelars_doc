#
# python pelars2rosbag.py INPUFTOLDE OUTPUR.bag
#
# Usage: 3 terminals
#
# roscore
# rosbag play OUTPUT.bag --rate 10
# rqt_image_view
#
#
# Design/Working Document: https://docs.google.com/document/d/1xfxWMQl17Wt_sRk3YkmG0wHhqOsrLfNblqRL2COGeZQ/edit
import json
import imageio
import rosbag
import numpy as np
import rospy
from std_msgs.msg import Int32, String
import argparse
import heapq
import os
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped,Pose,Point32
import tf_conversions
import sensor_msgs.msg

# from matrix 4x4 to TFMessage
def mat44totransform(mat44,t):
    pm = tf_conversions.toMsg(tf_conversions.fromMatrix(mat44))
    t.translation.x = pm.position.x
    t.translation.y = pm.position.y
    t.translation.z = pm.position.z

    t.rotation.x = pm.orientation.x
    t.rotation.y = pm.orientation.y
    t.rotation.z = pm.orientation.z
    t.rotation.w = pm.orientation.w

# Source: new packed and timestamped Kinect files from updated collector, RGB+D
# Output: rgb and depth channels of kinect
# Note: color is already JPEG so store as CompressedImage
# Note: if depth is ONI encoded need to decompress and recompress to Kinect (check work with K2 Windows)
#
#
# Message 
# http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
# topic /kinect/color/image
#      header.frame = "/kinect_rgb_frame"
# topic /kinect/color/image
#      header.frame = "/kinect_depth_frame"
#
# TODO: decide if registered or not => more work if not registered
class KinectReader:
    def __init__(self,path):
        pass
    def read(self):
        # return data and timestamp
        pass
    def next(self):
        pass
    def toros(self):
        # returns the encoded message
        pass

# Video file with timestamp file
#
# topic /kinect/color/image
#
# usare imageio per leggere i frame => OLD DATA => messy timestamping 
# NEW DATA => use anothe file for the timestamp
class VideoReaderStamped:
    def __init__(self,path):
        pass
    def read(self):
        # return data and timestamp
        pass
    def next(self):
        pass
    def toros(self):
        # returns the encoded message
        pass

# Video file with async and fixed rate
#
# topic /kinect/color/image
#
# usare imageio per leggere i frame => OLD DATA => messy timestamping 
# NEW DATA => use anothe file for the timestamp
class VideoReaderAsync:
    def __init__(self,path):
        pass
    def read(self):
        # return data and timestamp
        pass
    def next(self):
        pass
    def toros(self):
        # returns the encoded message
        pass

# Additional Data files TBD what content
class DataReader:
    def __init__(self,path):

        pass
    def read(self):
        # return data and timestamp
        pass
    def next(self):
        pass
    def toros(self):
        # returns the encoded message
        pass

# Stores the Camera Info
# http://docs.ros.org/api/sensor_msgs/html/msg/CameraInfo.html
class CameraInfoSource:
    # generate a static transformation with a given simulated rate
    def __init__(self,path,firsttime,topic,frame,cidata,rate):
        self.path = path
        self.firsttime = firsttime
        self.dt = 1000/rate
        self.topic = topic
        ci = sensor_msgs.msg.CameraInfo()
        self.ci = ci 
        self.current = firsttime

        ci.header.frame_id = frame
        # TODO fill in with real ci
    def read(self):
        # return data and timestamp
        return self.current,""
    def next(self):
        self.current += self.dt
    def toros(self):        
        self.ci.header.stamp = rospy.Time.from_sec(self.current/1000)
        return self.ci,self.topic

# Calibration information Source as 3 TF
class CalibrationSource:
    # generate a static transformation with a given simulated rate
    def __init__(self,path,firsttime,rate):
        self.path = path
        self.firsttime = firsttime
        self.dt = 1000/rate
        self.current = firsttime
    def read(self):
        # return data and timestamp
        return self.current,""
    def next(self):
        self.current += self.dt
    def toros(self):        
        alltf = []

        # returns the encoded message
        tb = TransformStamped()
        tb.header.frame_id = "/table"
        tb.header.stamp = rospy.Time.from_sec(self.current/1000)
        tb.child_frame_id = "/camera"
        m = np.identity(4)
        m[0:3,3] = (1,0,0)
        mat44totransform(m,tb.transform)
        alltf.append(tb)

        # returns the encoded message
        tb = TransformStamped()
        tb.header.frame_id = "/table"
        tb.header.stamp = rospy.Time.from_sec(self.current/1000)
        tb.child_frame_id = "/kinect"
        m = np.identity(4)
        m[0:3,3] = (1,1,0)
        mat44totransform(m,tb.transform)
        alltf.append(tb)

        tfm = TFMessage()
        tfm.transforms = alltf
        return tfm,"/tf"

# Simple image flow
class ImageReader:
    def __init__(self,path,frame,type,topic):
        fp = path + "_multimedias.json"
        self.data = json.load(open(fp,"rb"))
        self.current = -1
        self.folder = path
        self.frame = frame
        self.type = type
        self.topic = topic
        self.bridge = CvBridge()
        self.next()
    def read(self):
        if self.current != -1:
            return self.data[self.current]["time"],""
        else:
            return None,None
    def next(self):
        self.current += 1
        while self.current < len(self.data):
            if self.data[self.current]["view"] == self.type:
                return True
            else:   
                self.current += 1
        self.current = -1
        return False
    def toros(self):
        # returns the encoded message
        d = self.data[self.current]
        fp = os.path.join(self.folder,"media",d["data"].split("/")[-1] + "." + d["mimetype"])
        print fp 
        msg = None
        # TODO later without => codice from Fatigue PISA
        # CompressedImage message with bgr8 and JPEG 
        if d["mimetype"] == "jpg":
            msg = sensor_msgs.msg.CompressedImage()
            msg.data = open(fp,"rb").read()
            msg.format = "jpg"
        elif d["mimetype"] == "png":
            msg = sensor_msgs.msg.CompressedImage()
            msg.data = open(fp,"rb").read()
            msg.format = "png"

        if msg is None:
            img = cv2.imread(fp)
            msg = self.bridge.cv2_to_imgmsg(img, "bgr8"),"/image/"+ self.type

        # add header
        msg.header.stamp = rospy.Time.from_sec(self.current/1000)
        msg.header.frame_id = self.frame
        return msg,self.topic


def main():

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('folder')
    parser.add_argument('output')

    args = parser.parse_args()
    bag = rosbag.Bag(args.output, 'w')

    args.folder = args.folder.rstrip("/")

    sources = [
        ImageReader(args.folder,frame="/camera",type="people",topic="/camera/image/compressed"),
        #ImageReader(args.folder,"workspace"),
        #ImageReader(args.folder,"screem"),

        #KinectReader(args.folder),
        #DataReader(args.folder)
    ]

    myh = []

    for s in sources:
        t,d = s.read() # read current without advancing
        if t is not None:
            print "adding",t
            heapq.heappush(myh,(t,s))

    fixedratecount = 0

    if len(myh) != 0:
        fixedrates = []
        t0,s = myh[0] # FIRST
        #t,s = heapq.heappop(myh) # first time
        #heapq.heappush(myh,(t,s)) # push it back 
        cs = CalibrationSource(args.folder,t0,10)
        fixedrates.append(cs)

        cs = CameraInfoSource(args.folder,t0,"/kinect/color/camera_info","/kinect_rgb_frame",None,1) # TODO pass here the camera info information
        fixedrates.append(cs)

        #cs = CameraInfoSource(args.folder,"/kinect/depth/camera_info","/kinect",None,1) # TODO pass here the camera info information
        #fixedrates.append(cs)
        # IF kinect is NOT registered we need to publish /tf with the extrinsics calibration

        sources.extend(fixedrates) # starts from the first needed time

        for s in fixedrates:
            t,d = s.read()
            if t is not None:
                fixedratecount += 1
                heapq.heappush(myh,(t,s))
    else:
        print "no data"
        return 

    print "Initial",myh

    try:
        # assuming that we have only the calibration source
        while len(myh) != fixedratecount:
            t,s = heapq.heappop(myh)

            msg,topic = s.toros()
            #write(self, topic, msg, t=None, raw=False)
            # convert time from seconds to ros tyime
            bag.write(topic,msg,t=rospy.Time.from_sec(t/1000))

            s.next()
            t,d = s.read()
            if t is not None:
                heapq.heappush(myh,(t,s))
    finally:
        bag.close()

if __name__ == '__main__':
    main()