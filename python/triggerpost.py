import argparse,pelarslas

def makedef(session):
	ops =[
		dict(type="hand_speed"),
		dict(type="time_looking"),
		dict(type="media_info"),
		dict(type="proximity",table="Hand",statistics=True),
		dict(type="proximity",table="Face",statistics=True)
	]
	for x in ops:
		x["session"] = session
	group = dict(name="after session routine",session=session,operations=ops)
	return group


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Post Session')
	parser.add_argument('--clear',action="store_true")
	parser.add_argument('sessions',type=int,nargs="*")
	pelarslas.extendargs(parser)
	args = parser.parse_args()
	c = pelarslas.autoconnect(args)

	for session in args.sessions:
		if args.clear:
			c.delsessioncontent(session)
		names = c.getcontentnames(session)
		print names
		if False:
			#for i in range(0,10):
			#	on = "aftersession_" + str(session)
			#	if c.hascontent(session,on):
			#		c.delcontent(session,on)
			#		print "deleted",on
			for x in makedef(s)["operations"]:
				print "triggeringx",x
				c._doop(x)
