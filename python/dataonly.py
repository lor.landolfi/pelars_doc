from pelarslas import *
import pelarslas
import sys,json
import argparse
import time

if __name__ == "__main__":

	import argparse

	parser = argparse.ArgumentParser(description='Download Session')
	#parser.add_argument('--user')
	#parser.add_argument('--no-interactive',dest="interactive",action="store_false")
	#parser.add_argument('--interactive',dest="interactive",action="store_true")
	#parser.add_argument('--keychain',action="store_true")
	#parser.add_argument('--password')
	parser.add_argument('--dest',help="destination folder",default=".")
	parser.add_argument('--list',action="store_true")
	#parser.add_argument('--dummy',action="store_const",dest="user")
	#parser.add_argument('--goodlist',action="store_true",const="dummy@dummy.com")
	parser.add_argument('--force',action="store_true",help="forces download")
	parser.add_argument('sessions',type=int,nargs="*")

	pelarslas.extendargs(parser)
	args = parser.parse_args()
	c = pelarslas.autoconnect(args)
	
	fp = lambda x: os.path.join(args.dest,x)

	for sid in args.sessions:
		sid = int(sid)
		s = c.getsession(sid)
		k = fp("S%d_datas.json" % sid)
		t = time.time()
		a = c.getdatasraw(sid)
		t2 = time.time()
		print "got",len(a),"in",t2-t
		if args.force or not os.path.isfile(k):
			open(k,"w").write(a)




	# Precomputed
	# - aftersession_hand_speed
	# - aftersession_time_looking
	# - aftersession_media_info
	# - aftersession_hand_proximity
	# - aftersession_face_proximity
	# - aftersession_presence
