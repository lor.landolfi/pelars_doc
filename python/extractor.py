#
# Data extraction for PELARS based on downloaded JSON data
#
# Copyright Scuola Superiore Sant'Anna, Emanuele Ruffaldi 2016
#
import numpy as np
import re
import pandas
import json
import os
import sys
import math
import collections
import lmdb
import cPickle
import math
version = int(os.stat(os.path.abspath(__file__)).st_mtime/10.0)

# Takes a pandas dataset and transforms it into a matrix with first column the index of the dataset
# and the other columns the ones from Pandas
#
# as_matrix_with_index(DS(index,c1,..cN)) -> [index,c1,...,cN]
#
# Category: pandas utility
def as_matrix_with_index(DS):
    v = DS.values
    t = np.reshape(DS.index.values, (v.shape[0], 1))
    return np.hstack((t, v))

# Takes a sequence of pandas datasets and transforms them into a single dataset by stacking and
# reuising the common index field
def mergemany(list_DS):
    if len(list_DS) == 1:
        return list_DS
    m = list_DS[0]
    for i in range(1, len(list_DS)):
        m = pandas.merge(
            m, list_DS[i], left_index=True, right_index=True, how="outer")
    return m

# Takes a dataset x and computes the correct statistic for each based on the prefix
#
# mean_ => mean
# max   => max
# min   => min
# others => mean
def autostat(DS,t = None):
	if len(DS) > 1:
		actions = dict()
		for c in DS.columns:
			if c == t:
				continue
			if c.startswith("mean_"):
				actions[c] = np.mean
			elif c.startswith("max_"):
				actions[c] = np.max
			elif c.startswith("min_"):
				actions[c] = np.min
			else:
				actions[c] = np.mean
				print "unknown action for",c
		return DS.groupby(t).agg(actions)
	else:
		return DS

# takes the columns of the dataset and appends prefix to all of them using rename
# it works inplace
def addprefix(DS, prefix, inplace = True):
    a = DS.columns
    b = [prefix + x for x in a]
    return DS.rename(columns=dict(zip(a, b)), inplace=inplace)

# filters the dataset in TIME by selecting the given phase (or None)
# the filtering is based on the field "time" or the index
#
# phase is a dictionary with "start" and "end" inclusive
def filterbyphase(DS, phase=None):
    if phase is None:
        return DS
    else:
        if not "time" in DS.columns:
            tt = DS.index
        else:
            tt = DS["time"]
        return DS[(tt >= phase["start"]) & (tt <= phase["end"])]

# adds a field named newfieldname obtained by discrete windowing the column "time"
# or the index using the reference time ref
#
# TODO avoid tolist
def addtimewindow(DS, newfieldname, windowsize_timeunits, reference_time):
    if not "time" in DS.columns:
        tt = DS.index
    else:
        tt = DS["time"]
    if reference_time is None:
        DS[newfieldname] = [math.floor(x/windowsize_timeunits)*windowsize_timeunits for x in tt.tolist()]
    else:
        DS[newfieldname] = [math.floor((x-reference_time)/windowsize_timeunits)*windowsize_timeunits for x in tt.tolist()]
    return DS


def parseide(x):
    # action_id: M1 M2 ===> [A-Z]+[0-9]*
    # opt: split by space
    #	opt[0] number
    #	opt[1] what
    #
    # what: set([u'IFdefaultValB', u'Map', u'MapfromHigher', u'Triggerpos', u'MTRpos', u'MTR', u'MapfromLower', u'ACRpos', u'RGBpos', u'IFuseDefaultB', u'MaptoLower', u'MaptoHigher', u'ACR', u'Mappos', u'Trigger', u'IFpos', u'TriggerprimeVal', u'IFcondition', u'RGB', u'COL', u'IF'])
    # action first: set([u'CC', u'B', u'M', u'L'])
    # complexity:
    #
    # names starting M: hardware
    # names starting B: software
    # names starting CC: software manipulation
    # names starting L: linkage
    #
    #
    # Outcome of the parsing:
    #
    m = re.match("([A-Z]+)(\d*)", x["action_id"])
    context = m.group(1)
    #subaction = m.group(2)
    # prefix.add(m.group(1))
    a = x["opt"].split()
    # s.add(a[1])
    a[0] = int(a[0])
    if context == "L":
        return dict(action="link", action_id=x["action_id"], opt=x["opt"])
        # (id fromname) (id toname) (fromport toport)
        # print x["action_id"],x["opt"]
        pass
    if context == "CC":
        # (id what value)
        return dict(action="change", action_id=x["action_id"], opt=x["opt"], type=a[1], id=a[0])
    # print m.group(2)
    if context == "B":
        return dict(action="block", id=a[0], opt=x["opt"], type=a[1])
    if context == "M":
        return dict(action="module", id=a[0], opt=x["opt"], type=a[1])
    else:
        return dict(action="")


def distance(a, b):
    return math.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2+(a[2]-b[2])**2)


def distance2(a, b):
    return math.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)


def list2panda(w):
    return pandas.DataFrame.from_records(w)


class NoGrouper:

    def __init__(self, ex):
        self.ex = ex

    def iter(self, what):
        yield "", what

class Face:

	def __init__(self, features, eyeclosure_left, eyeclosure_right, facescale, gaze_direction, pose, time):
		self.features = features
		self.eyeclosure_left = eyeclosure_left
		self.eyeclosure_right = eyeclosure_right
		self.facescale = facescale
		self.gaze_direction = gaze_direction
		self.rotation = pose["rotation"]
		self.translation = pose["translation"]
		self.time = time

class PhaseGrouper:

    def __init__(self, ex):
        self.ex = ex
        self.phases = self.ex.phases()

    def iter(self, what):
        w = []
        ip = 0
        ph = self.phases.iloc[ip]
        pn = ph.phase
        last = ph.end
        first = ph.start
        for index, x in what.iterrows():
            if x.time > last:
                yield (pn, first, last), list2panda(w)
                w = []
                ip += 1
                if ip >= len(self.phases):
                    pn = ""
                    last = float("inf")
                    first = x.time
                else:
                    ph = self.phases.iloc[ip]
                    pn = ph.phase
                    last = ph.end
                    first = ph.start
            w.append(x)
        if len(w) > 0:
            last = w[-1].time
            yield (pn, first, last), list2panda(w)


class WindowGrouper:

    def __init__(self, ex, mssize, skipempty=False):
        self.ex = ex
        self.skipempty = skipempty
        self.mssize = mssize

    def iter(self, what):
        w = []
        first = None
        next = None
        last = 0
        for index, x in what.iterrows():
            if first is None:
                first = x.time
                next = x.time + self.mssize
            elif x.time >= next:
                while x.time >= next:
                    if len(w) > 0 or not self.skipempty:
                        yield ("", first, next), list2panda(w)
                        w = []
                    first = next
                    next += self.mssize
            w.append(x)
        # last one
        if len(w) > 0:
            yield ("", first, next), list2panda(w)


class OverlapGrouper:

    def __init__(self, ex, msoverlap, msrest, skipempty=False):
        self.ex = ex
        self.skipempty = skipempty
        self.msoverlap = msoverlap
        self.msrest = msrest
        self.mssize = msrest+msoverlap

    def iter(self, what):
        w = []
        first = None
        next = None
        last = 0
        for index, x in what.iterrows():
            if first is None:
                # [NO OV]
                first = x.time
                next = x.time + self.mssize
            elif x.time >= next:
                while x.time >= next:
                    if len(w) > 0 or not self.skipempty:
                        yield ("", first, next), list2panda(w)
                        if len(w) > 0:
                            # [NO  OV] NO
                            #     [OV  NO]
                            keep = first + self.msrest
                            w = [y for y in w if y.time >= keep]
                        else:
                            w = []
                    # [NO  OV] NO
                    #     [OV  NO]
                    first += self.msrest
                    next += self.msrest
            w.append(x)
        # last one
        if len(w) > 0:
            yield ("", first, next), list2panda(w)


class TimeTool:

    def __init__(self, threshold=1200):
        self.active = 0
        self.threshold = threshold
        self.total = 0

    def run(self, data):
        for i, x in data.iterrows():
            if i == 0:
                continue
            dt = x.time-data.iloc[i-1].time
            if dt > 0 and dt < self.threshold:
                self.active += dt
            self.total += dt


class Extractor:
	def __init__(self,session,reltime=False,path=None):
		if path is None:
			path = os.getenv("PELARSDATAPATH")			
		self.path = path
		self.session = session
		self.timerange = None
		self.datafile = None
		self.multimedia = None
		if session is not None:
			j = json.load(open(os.path.join(self.path,"S%d_session.json" % self.session),"rb"))
			self.starttime =float(j["start"])
			self.meta = j
		else:
			self.meta = dict()
			self.starttime = 0
		self.reltime = reltime and self.starttime or 0


	def loadJsonFaces(self, json_folder, session, connection):
		faces = []
		for session_folder in os.listdir(json_folder):
			if int(session_folder.split(".")[0]) == session:
				local_folder = os.path.join(json_folder,session_folder)
				for multimedia in os.listdir(local_folder):
					content = json.loads(open(os.path.join(local_folder,multimedia),"rb").read())
					meta_data = connection.getMeta(session, multimedia.split(".")[0])
					time = meta_data["time"]
					if str(content) != "[]":
						for obj in content:
							faces.append(Face(obj["2Dfeatures"], obj["eyeclosure_left"], obj["eyeclosure_right"], obj["facescale"], obj["gaze_direction"], obj["pose"],time ))
		return faces			
			


	def sessionmeta(self):
		return self.meta
	def sessionscores(self):
		scoremap = dict(good=3,ok=2,bad=1,bad_ok=1.5)
		#SessionID	PLAN	RESULT	OVERALL	TEAM
		cvt = dict()
		cvt[0] = int
		cvt[1] = lambda x: scoremap[x]
		cvt[2] = cvt[3] = cvt[1]	
		cvt[4] = lambda x: ord(x)-ord("A")+1
		r = pandas.read_csv(os.path.join(self.path,"session_scores.csv"),converters=cvt,sep="\t")
		r.rename(columns=dict(SessionID="session"),inplace=True)
		r = r.set_index("session")
		a = r.columns
		r.rename(columns=dict(zip(a,[x.lower() for x in a])),inplace=True)
		return r

	def sessionscores2(self):
		#SessionID	check	clarity	indip_thinking	plan_corr	working	quality	own_grade
		cvt = dict()
		for i in range(0,8):
			cvt[i] = int
		cvt[1] = str
		r = pandas.read_csv(os.path.join(self.path,"session_scoresV2.csv"),converters=cvt,sep="\t")
		r.rename(columns=dict(SessionID="session"),inplace=True)
		r = r.set_index("session")
		a = r.columns
		r.rename(columns=dict(zip(a,[x.lower() for x in a])),inplace=True)
		return r
	def sessioncpsscores(self):
		def pct(x):
			return float(x.rstrip("%").replace(",","."))
		def score(x):
			scoremap = dict(medium=2,low=1,high=3)
			scoremap["medium/low"] = 1.5
			scoremap["high/medium"] = 2.5
			scoremap["medium/high"] = 2.5
			return scoremap[x.rstrip("/").lower()]
		#SessionID	PLAN	RESULT	OVERALL	TEAM
		cvt = dict([(i,w) for i,w in enumerate([int,pct,score,pct,pct,pct,score,pct,pct,pct,score,score])])

		r = pandas.read_csv(os.path.join(self.path,"session_scores_cps.tsv"),converters=cvt,sep="\t")
		cd = dict(SessionID="session")
		r.rename(columns=cd,inplace=True)
		r = r.set_index("session")
		a = r.columns
		r.rename(columns=dict(zip(a,[x.lower() for x in a])),inplace=True)
		return r
	def _loaddatafile(self):
		if self.datafile is None:
			self.datafile = json.load(open(os.path.join(self.path,"S%d_datas.json" % self.session),"rb"))
			if self.timerange is not None:
				# TODO optimize				
				tr = self.timerange
				self.datafile = [x for x in self.datafile if x["time"] >= tr[0] and x["time"] <= tr[1]]
	def loadfile(self,x):
		return json.load(open(os.path.join(self.path,x),"rb"))
	def _loadmultimedia(self):
		if self.multimedia is None:
			self.multimedia = json.load(open(os.path.join(self.path,"S%d_multimedias.json" % self.session),"rb"))

	def handsmerge(self):
		self._loaddatafile()
		#data_id num tx ty tz rx ry rz time
		q = []
		lasttime = None
		w = []
		ff = "time num tx ty tz rx ry rz rw".split(" ")
		for x in self.datafile:
			if x["type"] == "hand":
				t = x["time"]-self.reltime
				if lasttime is not None:
					if lasttime != t:
						z = pandas.DataFrame(w,columns = ff)						
						z.set_index("num")
						#z = lambda : pandas.DataFrame(w,columns=ff)
						q.append((lasttime,z))
						w = []		
				w.append([t,x["num"],x["tx"],x["ty"],x["tz"],x["rx"],x["ry"],x["rz"],x["rw"]])
				lasttime = t
		if len(w) != 0:
			#z = lambda : pandas.DataFrame(w,columns=ff)
			z = pandas.DataFrame(w,columns = ff)
			z.set_index("num")
			q.append((lasttime,z))
		r = pandas.DataFrame(q,columns=["time","data"])
		#r.set_index("time")
		return r


	def hands(self):
		self._loaddatafile()
		q = []
		for x in self.datafile:
			if x["type"] == "hand":
				t = x["time"]-self.reltime
				q.append([t,x["num"],x["tx"],x["ty"],x["tz"],x["rx"],x["ry"],x["rz"],x["rw"]])
		return pandas.DataFrame(q,columns="time num tx ty tz rx ry rz rw".split(" "))
		
	def faceset(self):
		self._loaddatafile()
		return collections.Counter([x["num"] for x in self.datafile if x["type"] == "face"])
	def handset(self):
		self._loaddatafile()
		return collections.Counter([x["num"] for x in self.datafile if x["type"] == "hand"])
	def buttons(self):
		self._loaddatafile()
		button2id = dict(b2=1)
		q = [   [x["time"]-self.reltime,button2id.get(x["data"],0)]  for  x in self.datafile if x["type"] == "button"]
		return pandas.DataFrame(q ,columns=("time","button"))
	def ides(self):
		self._loaddatafile()
		return pandas.DataFrame([(x["time"]-self.reltime,x["action_id"]) for x in self.datafile if x["type"] == "ide"],
				columns = ("time","action"))
	def phases(self,withreltime):
		l = dict(document="work")
		j = json.load(open(os.path.join(self.path,"S%d_phases.json" % self.session),"rb"))
		if withreltime:
			q = [   [x["start"]-self.reltime,x["end"]-self.reltime,(x["end"]-x["start"]),l.get(x["phase"].replace("obs_",""),x["phase"].replace("obs_",""))]  for  x in j]
		else:
			q = [   [x["start"],x["end"],(x["end"]-x["start"]),l.get(x["phase"].replace("obs_",""),x["phase"].replace("obs_",""))]  for  x in j]
		return pandas.DataFrame(q ,columns=("start","end","duration","phase"))
	def filtertime(self,fromx,to):
		if to < fromx:
			self.timerange = None
		else:
			print "filtertime",fromx,"-",to,"for",(to-fromx)/1E3,"seconds"
			self.timerange = (fromx,to)
	def firstphaseof(self,what):
		p = self.phases(withreltime=False)
		return p.index.values[min([i for i in range(0,len(p)) if p.phase[i] == what],key=lambda j: p.start[j])]
	def endphaseof(self,what):
		p = self.phases(withreltime=False)
		return p.index.values[max([i for i in range(0,len(p)) if p.phase[i] == what],key=lambda j: p.end[j])]

	def normalizedphases(self,asdict=False,withreltime=False):
		p = self.phases(withreltime=withreltime)
		
		if self.session == 1568:
			plan0 = self.firstphaseof("plan")
			plan1 = plan0
		else:
			plan0 = self.firstphaseof("plan")
			plan1 = self.endphaseof("plan")
		if self.session == 1600:
			reflect1 = self.endphaseof("reflect")
			reflect0 = reflect1
		else:
			reflect0 = self.firstphaseof("reflect")
			reflect1 = self.endphaseof("reflect")
		work = self.firstphaseof("work")
		#print "result",plan0,plan1,reflect0,reflect1,work
		plan = plan0
		reflect = reflect0
		p.set_value(plan,'end', p.end[plan1])
		p.set_value(reflect,'end', p.end[reflect])
		p.set_value(work,'start', p.end[plan])
		p.set_value(work,'end', p.start[reflect])


		r = p.iloc[[plan,reflect,work],:]
		r.duration = r.end-r.start

		if asdict:
			r = r.set_index("phase")
			return r.to_dict(orient="index")
		else:
			return r

	def audios(self):
		self._loaddatafile()
		return pandas.DataFrame([(x["time"]-self.reltime,x["value"]) for x in self.datafile if x["type"] == "audio"],columns=("time","level")).set_index("time")
	def datatypes(self):
		self._loaddatafile()
		return set([x["type"] for x in self.datafile])
	def datatimings(self):
		self._loaddatafile()
		w = dict(ide=0,hand=1,face=2,audio=3,button=4)
		return [(x["time"],w[x["type"]]) for x in self.datafile]
	def usergens(self):
		self._loadmultimedia()
		maptype = dict(text=1,image=2,video=3)
		return pandas.DataFrame([(x["time"]-self.reltime,maptype.get(x["type"],0),x["size"]) for x in self.multimedia if x["creator"] == "student"],				columns = "time type size".split(" "))
	def _cachedcontent(self,prefix,fx):
		global version
		#data_id num tx ty tz rx ry rz time
		env = lmdb.open('mylmdb')
		with env.begin(write=True) as txn:
			v = float(txn.get("%s_version_%d" % (prefix,self.session),"0"))
			if v == version:
				r = txn.get("%s_%d"% (prefix,self.session))
				if r is not None:
					print "cached",prefix,self.session
					return pandas.read_msgpack(r)
			else:
				print "creating ",prefix,"for",self.session,"with version",v,"vs",version
				r = fx()
				txn.put("%s_%d" % (prefix,self.session), r.to_msgpack(None))
				txn.put("%s_version_%d" % (prefix,self.session), str(version))
				return r
	def facesreal(self):
		self._loaddatafile()
		q = []
		for x in self.datafile:
			if x["type"] == "face":
				# FACE WAS in camera coordinates as a rectangle: centroid and area are preserved
				p0 = (x["pos_x0"],x["pos_y0"],x["pos_z0"])
				p1 = (x["pos_x1"],x["pos_y1"],x["pos_z1"])
				p2 = (x["pos_x2"],x["pos_y2"],x["pos_z2"])
				cx,cy,cz = [(p0[i]+p1[i]+p2[i])/3 for i in range(0,3)]
				area = distance(p1,p0)*distance(p2,p0)
				q.append([x["time"]-self.reltime,x["num"],x["distance"],cx,cy,cz,area]) #x["pos_x0"],x["pos_x1"],x["pos_x2"],x["pos_y0"],x["pos_y1"],x["pos_y2"],x["pos_x2"],x["pos_z0"],x["pos_z1"],x["pos_z2"]])
		r =  pandas.DataFrame(q,columns="time num distance cx cy cz area".split(" ")) #x0 x1 x2 y0 y1 y2 z0 z1 z2".split(" "))
		return r

	def faces(self):
		#return self._cachedcontent("faces",self.facesreal)
		return self.facesreal()

	def facesdistance(self):
		def processframe(r,x):
			if len(x) == 3:
				t = x[0]["time"]
				md = (distance2(x[0]["c"],x[1]["c"])+distance2(x[0]["c"],x[2]["c"])+distance2(x[1]["c"],x[2]["c"]))/3
				r.append((t-self.reltime,md))
			else:
				return
		r = []
		#"max_distance","min_distance"
		self._facestimed(lambda x: processframe(r,x))
		return pandas.DataFrame(r,columns=["time","mean_distance"]).set_index("time")


	def visagefaces(self, path, session, connection):
		faces = self.loadJsonFaces(path, session, connection)
		print "\tLoaded " + str(len(faces)) + " faces"
		#self.features = features
		#self.eyeclosure_left = eyeclosure_left
		#self.eyeclosure_right = eyeclosure_right
		#self.facescale = facescale
		#self.gaze_direction = gaze_direction
		#self.rotation = pose["rotation"]
		#self.translation = pose["translation"]
		#self.time = time
		r = []
		for face in faces:
			time = face.time -self.reltime
			trans = face.translation
			rot = face.rotation
			scale = face.facescale
			reye = 1.0 if face.eyeclosure_right else 0.0  #This is in order to have a meaningful mean value
			leye = 1.0 if face.eyeclosure_left else 0.0
			r.append((time, trans["x"], trans["y"], trans["z"], rot["x"], rot["y"], rot["z"], rot["w"], scale, reye, leye))
		return pandas.DataFrame(r,columns=["time","tx", "ty", "tz", "rx", "ry", "rz", "rw", "scale", "reye", "leye"]).set_index("time")


	def handsdistance(self):
		def processframe(r,x):
			# generically compute centroid
			w = [] # distance pairs among hands
			for i in range(0,len(x)):
				for j in range(i+1,len(x)):
					w.append(distance(x[i]["c"],x[j]["c"])) # it was sum
			if len(w) > 0:
				wM = max(w)
				wm = min(w)
				w = sum(w)/len(w)
				t = x[0]["time"]
			else:
				wM = 0
				wm = 0
				w = 2
				t = x[0]["time"]
			r.append((t-self.reltime,w,wM,wm))
		r = []
		self._handstimed(lambda x: processframe(r,x))
		return pandas.DataFrame(r,columns=["time","mean_distance","max_distance","min_distance"]).set_index("time")


	def handscentroids(self):
		def processframe(r,x):
			# generically compute centroid
			wtx = list(j["tx"] for j in x)
			wty = list(j["ty"] for j in x)
			wtz = list(j["tz"] for j in x)
			n = len(wtx)
			t = x[0]["time"]
			r.append((t-self.reltime,sum(wtx)/n,sum(wty)/n,sum(wtz)/n))
		r = []
		self._handstimed(lambda x: processframe(r,x))
		return pandas.DataFrame(r,columns=["time","mean_tx","mean_ty","mean_tz"]).set_index("time")


		
	def handsspeed(self):
		def processframe(r,rs,xx):
			# generically compute centroid
			allt = []
			ct = 0
			for x in xx:
				id = x["num"]
				last = rs.get(id)
				if last is not None:
					lasttime,lastpos = last
					dt = x["time"]-lasttime
					dp = distance(lastpos,x["c"])
					allt.append(dp/(dt/1E3)) #
					ct = x["time"]
				rs[id] = (x["time"],x["c"])
			if len(allt) > 0:
				wM = max(allt)
				wu = sum(allt)/len(allt)
				r.append((ct-self.reltime,wu,wM))
		r = []
		rs = {}
		self._handstimed(lambda x: processframe(r,rs,x))
		return pandas.DataFrame(r,columns=["time","mean_speed","max_speed"]).set_index("time")

	def facescount(self):
		r = []
		def process(r,x):
			t = x[0]["time"]
			r.append((t-self.reltime,len(x)))
		self._facestimed(lambda x: process(r,x))
		return pandas.DataFrame(r,columns=["time","face_count"]).set_index("time")

	def idecount(self):
		r = []
		def process(r,x):
			t = x[0]["time"]
			r.append((t-self.reltime,len(x)))
		self._idetimed(lambda x: process(r,x))
		return pandas.DataFrame(r,columns=["time","ide_count"]).set_index("time")

	def ideeval(self):
		r = []
		def process(r,x):
			t = x[0]["time"]
			pi = [parseide(y) for y in x]
			hw = len(set([y["type"] for y in pi if y["action"] == "module"]))
			sw = len(set([y["type"] for y in pi if y["action"] == "block"]))
			cc = len([y for y in pi if y["action"] == "link"])
			r.append((t-self.reltime,hw,sw,cc))
		self._idetimed(lambda x: process(r,x))
		return pandas.DataFrame(r,columns=["time","hwvariety","swvariety","complexity"]).set_index("time")

	def _idetimed(self,fx):
		self._loaddatafile()
		q = []
		lasttime = None
		for x in self.datafile:
			if x["type"] == "ide":
				if x["time"] != lasttime:
					if len(q) > 0:
						fx(q)
					q = [x]
					lasttime = x["time"]
				else:
					q.append(x)
		if len(q) != 0:
			fx(q)	
	def _facestimed(self,fx):
		self._loaddatafile()
		q = []
		lasttime = None
		for x in self.datafile:
			if x["type"] == "face":
				# three points
				p0 = (x["pos_x0"],x["pos_y0"],x["pos_z0"])
				p1 = (x["pos_x1"],x["pos_y1"],x["pos_z1"])
				p2 = (x["pos_x2"],x["pos_y2"],x["pos_z2"])
				# averaged then in tomb
				x["c"] = tuple([(p0[i]+p1[i]+p2[i])/3 for i in range(0,3)])
				if x["time"] != lasttime:
					if len(q) > 0:
						fx(q)
					q = [x]
					lasttime = x["time"]
				else:
					q.append(x)
		if len(q) != 0:
			fx(q)
	def hands(self):
		r = []
		for x in self.datafile:
			if x["type"] == "hand":
				r.append((x["time"],x["num"],x["tx"],x["ty"],x["tz"]))
		return np.array(r,dtype={'names':['time', 'num','tx','ty','tz'], 'formats':['f4','f4','f4','f4','f4']})
	def handsconcurrent(self):
		r = []
		def fx(r,x):
			r.append((x[0]["time"],len(x)))
		self._handstimed(lambda x: fx(r,x))
		return np.array(r,dtype={'names':['time', 'count'], 'formats':['f4','f4']})

	def _handstimed(self,fx):
		self._loaddatafile()
		q = []
		lasttime = None
		for x in self.datafile:
			if x["type"] == "hand":
				x["c"] = (x["tx"],x["ty"],x["tz"])
				if x["time"] != lasttime:
					if len(q) > 0:
						fx(q)
					q = [x]
					lasttime = x["time"]
				else:
					q.append(x)
		if len(q) != 0:
			fx(q)		
