
import os,sys,argparse,json
from subprocess import check_output
import itertools,pprint
import numpy as np
from collections import defaultdict


def callaruco(calibfile,markersize,infile):
    
    #calibrationfile size imagefile alreadyundist [-|outfilename]
    pa = "/Users/eruffaldi/Dropbox/repos/aruco2json/build/aruco2json"
    zz = "\"%s\" \"%s\" %f \"%s\" 0" % (pa,calibfile,markersize,infile)
    os.system(zz)
    outfile = infile + ".json"
    if os.path.isfile(outfile):
        return outfile
    else:
        return None

# Calibration: /calibration
# calibration_webcam.xml
class Calibration:
    def __init__(self,d):
        self.session = d["session"]
        self.type = d["type"]
        #self.width = d["width"]
        #self.height = d["height"]
        c = np.array(d["parameters"])
        self.rot = np.reshape(c[0:9],(3,3))
        self.pos = np.reshape(c[9:],(3,1))
    def __repr__(self):
        return "Calibration(%s,%s,%s)" % (self.type,self.pos,self.rot)

# Calibration: /calibration
# calibration_webcam.xml
class Intrinsics:
    def __init__(self,d):
        if type(d) is dict:
            self.session = d["session"]
            self.type = d["type"]
            #self.width = d["width"]
            #self.height = d["height"]
            c = np.array(d["parameters"])
            self.rot = np.reshape(c[0:9],(3,3))
            self.pos = np.reshape(c[9:],(3,1))
        else:
            y = yaml.load(open(d,"rb"))
            # rgb_intrinsics
            # rgb_distortion

    def __repr__(self):
        return "Intrinsics()"
    def flipxpoint2(self,p):
        return (self.width-p[0],p[1])
    def flipxpoint3(self,p):
        X = p[0]
        Y = p[1]
        Z = p[2]
        w = self.width
        cx = self.K[0,2]
        fx = self.K[0,0]
        return (-(2*Z*cx - w + X*fx)/fx,Y,Z)
        
    def flipxpose(self,pose):
        pp = [(0,0,0,1),(1,0,0,1),(0,1,0,1)]
        pass
"""

Si scorrono i dati degli snapshot automatici e si emettono queste trasformazioni, una per ogni istante:

- c920 to table
- kinect to table
- head # to c920
- eye # to c920 (esportiamo eye frame? se no si modifica e si rilancia)
- hand # to kinect

Utilizzi? in particolare debugging della situazione, visualizzazione cool (RViz in ROS o Three.js). Lo utilizzer per il paper

Formato JSON, matrice 4x4 oppure (pos+quat):

output: JSON array of frame

frame:  JSON dictionary
- timestamp (tempo)
- transforms: JSON array of transform

transform: JSON dictionary
- parent: string name
- child: string name
- position: [x,y,z]
- quaternion: [x,y,z,w]

Sources:
/Volumes/BigData/PELARS/sessions/pelarspeoplejson-2.75/SESSION/
      "gaze_direction" : {
         "x" : -0.13831876218318939,
         "y" : 0.29530161619186401,
         "z" : -0.084229536354541779
      },
      "pose" : {
         "rotation" : {
            "w" : 0.97431200742721558,
            "x" : -0.020837703719735146,
            "y" : -0.10588197410106659,
            "z" : 0.19766370952129364
         },
         "translation" : {
            "x" : 0.20261536538600922,
            "y" : 0.10552120208740234,
            "z" : 1.2452068328857422
         }

SSESSION_calibration.json
[{"session": 1542, "type": "webcam", "parameters": [0.02686176635324955, 0.9995942711830139, -0.009476004168391228, -0.1945860534906387, 0.1656122803688049, -0.01379847154021263, -0.9860944151878357, 0.3753648698329926, -0.9858250617980957, 0.02491889335215092, -0.1659157425165176, 1.257906198501587]}, {"session": 1542, "type": "kinect2", "parameters": [-0.1170692145824432, 0.9931235909461975, 0.0006015668041072786, 0.07496095448732376, 0.7951939702033997, 0.09410014748573303, -0.5990089774131775, -0.4207692742347717, -0.5949465036392212, -0.06964714080095291, -0.8007420897483826, 1.497772574424744]}]

SSESSION_datas.json
getting images: SSESSION_multimedias.json
 {"mimetype": "jpg", "type": "image", "creator": "client", "trigger": "automatic", "session": 1542, "time": 1467791763795.0, "view": "workspace", "data": "/multimedia/1542/10610", "id": 10610, "size": 655484}
 {"mimetype": "jpg", "type": "image", "creator": "client", "trigger": "automatic", "session": 1542, "time": 1467791822333.0, "view": "people", "data": "/multimedia/1542/10612", "id": 10612, "size": 106897}

getting hand:
{"session": 1542, "rw": 0.923536, "tz": 0.153164, "tx": 0.0842537, "data_id": 631501, "ty": -0.361422, "rx": 0.025635, "ry": 0.381077, "rz": -0.034702, "num": 14, "time": 1467791704683.0, "type": "hand"}

verification of face count: 
{"session": 1542, "pos_x2": 0.110675, "pos_x1": 0.141354, "pos_x0": 0.105706, "pos_y0": -0.526497, "pos_y1": -0.708865, "pos_y2": -0.711421, "distance": 1.058557152748108, "data_id": 631487, "num": 0, "time": 1467791703446.0, "pos_z1": 0.468087, "pos_z0": 0.652507, "pos_z2": 0.650754, "type": "face"}

"""

class ApproxSync:
    def __init__(self,keys,slops_ms,queue_size):
        self.listener = lambda x: 0
        self.iqueues = dict([(k,i) for i,k in enumerate(keys)])
        self.queues = [{} for k in keys]
        self.slops_ms = slops_ms
        self.discarded = dict([(k,0) for i,k in enumerate(keys)])
        self.queue_size = queue_size
        self.seen = dict([(k,0) for i,k in enumerate(keys)])
    def signalMessage(self,msgs):
        self.listener(msgs)
    #https://github.com/strawlab/image_pipeline/blob/master/camera_calibration/src/camera_calibration/approxsync.py
    def push(self,key,time_sec,content):
        # TODO assume always latest
        q = self.queues[self.iqueues[key]]
        q[time_sec] = content
        d = 0
        self.seen[key] += 1
        while len(q) > self.queue_size:
            del q[min(q)]       
            d += 1
        if d > 0:
            self.discarded[key] += d
        for vv in itertools.product(*[q.keys() for q in self.queues]):
            if ((max(vv) - min(vv)) < self.slops_ms):
                msgs = [q[t] for q,t in zip(self.queues, vv)]
                self.signalMessage(msgs)
                for q,t in zip(self.queues, vv):
                    try:
                        del q[t]
                    except KeyError:
                        pass # TODO: why can del q[t] fail?

def picksessions(args,p):
    allsessions = []
    if not args.good and not args.ucv:
        print "ALL downloaded"
        for s in os.listdir(p):
            fp = os.path.join(p,s)
            if s[0] == "S" and os.path.isdir(fp):
                allsessions.append(int(s[1:]))
    elif not args.ucv:
        print "GOOD only"
        for s in json.load(open(os.path.join(p,"goodsessions.json"),"rb")):
            allsessions.append(s["session"])
    else:
        print "UCV"
        for q in open(os.path.join(p,"session_scores.csv"),"rb"):
            a = q.split("\t")
            if a[0] != "SessionID":
                allsessions.append(int(a[0]))
    return allsessions

class MergeSameTime:
    def __init__(self,data):
        self.data = data
        self.index = 0
    def next(self):
        r = []
        t = None
        while self.index < len(self.data):
            x = self.data[self.index]
            if t is None:
                r.append(x)
                self.index += 1
                t = x["time"]
            elif t != x["time"]:
                yield (t,r)
                r = []
                t = None
            else:
                r.append(x)
                self.index += 1
        if t is not None:
            yield (t,r)



class ForwardSearcher:
    def __init__(self,data):
        self.data = data
        self.index = 0
    def next(self,t):
        while True:
            tnow = self.data[self.index][0]
            if self.index < len(self.data)-1:
                tnext = self.data[self.index+1][0]
                if t > tnext:
                    self.index += 1
                else:
                    return (tnow,self.data[self.index])
            else:
                if t > tnow:
                    return (None,None)
                else:
                    return (tnow,self.data[self.index])

def main():
    os.environ["DYLD_LIBRARY_PATH"] = os.environ["DYLD_LIBRARY_PATH"] + ":" + "/Users/eruffaldi/Dropbox/repos/aruco2json/build"
	   
    print os.environ["DYLD_LIBRARY_PATH"]
    parser = argparse.ArgumentParser(description='Process some integers.')
    #parser.add_argument('--good',action="store_true")

    p = os.environ.get("PELARSDATA","/Volumes/BigData/PELARS/sessions")
    parser.add_argument('--good',action="store_true")
    parser.add_argument('--ucv',action="store_true")

    args = parser.parse_args()
    p = os.environ.get("PELARSDATA","/Volumes/BigData/PELARS/sessions")
    allsessions = picksessions(args,p)
    markersize = 0.035
    c920calib = os.path.abspath("c920_800.yml")
    k2calib = os.path.abspath("k2.yml")
    c920flip = True
    for s in allsessions:
        fp = os.path.join(p,"S%d" % s)
        if os.path.isdir(fp):
            mm = fp + "_multimedias.json"
            dd = fp + "_datas.json"
            outdir = os.path.join(fp,"aruco")
            if not os.path.isdir(outdir):
                os.mkdir(outdir)
            if os.path.isfile(mm) and os.path.isfile(dd):
                wm = json.load(open(mm,"rb"))
                approxsync = ApproxSync(("people","workspace","screen"),slops_ms=30000,queue_size=3)

                approxsync.listener = lambda x: aligned.append((x[0]["time"],x))
                aligned = []

                # camerax_marker
                calibs = dict([(d["type"],Calibration(d)) for d in json.load(open(fp+"_calibration.json","rb"))])
                print calibs
                for x in wm:
                    if not ("view" in x and x.get("trigger","") == "automatic"):
                        continue
                    v = x["view"]
                    t = x["time"]
                    id = x["id"]
                    content = "%d.%s" % (id,x["mimetype"])
                    x["content"] = content
                    if not os.path.isfile(os.path.join(fp,"media",content)):
                        print "missing",content
                    else:
                        approxsync.push(v,int(t),x)
                allhands = defaultdict(list)
                for x in [x for x in json.load(open(dd,"rb")) if x["type"] == "hand"]:
                    allhands[int(x["time"])].append(x)

                mint = min(allhands.keys())
                print "firsthands",allhands[mint]
                # c920 is flipped => work on c920 with flipping => cannot relate with K2
                for t,q in aligned:
                    people,workspace,screen = q
                    images = [(c920calib,people,"w"),(k2calib,workspace,"p")]
                    for cal,img,what in images:
                        imfile = os.path.join(fp,"media",img["content"])
                        jfile = imfile +".json"
                        if not os.path.isfile(jfile):
                            print "aruco",imfile
                            callaruco(cal,markersize,imfile)
                        if os.path.isfile(jfile):
                            # markers are in TABLE frame at given time
                            print s,int(t),int(img["time"]),what,img["content"],len(json.load(open(jfile,"rb"))["markers"])
                            if cal == c920calib:
                                xt = int(img["time"])                                
                                h = json.load(open(jfile,"rb"))
                                print "json hands new",h
                            # for the c920 we want to associate

                    print "-"

                # what we publish . due to the sync issues and the problem of flipping we send
                # - 
                if False:
                    wd = [x for x in json.load(open(dd,"rb")) if x["type"] == "hand"]
                    fs = ForwardSearcher(aligned)

                    for t,h in MergeSameTime(wd).next():
                        xt,xm = fs.next(t)
                        print t,xt,(xt-t)
                else:
                    for t,d in aligned:
                        pass




                # we have aligned with their timing and hands with their




            


if __name__ == '__main__':
	main()