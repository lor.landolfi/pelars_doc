from requests import Session
import json,os,pprint,sys,getpass

def gettoken(user,password):
	session = Session()
	r = session.post('http://pelars.sssup.it/pelars/password',params={"user":user,"pwd":password})
	if r.status_code != 200:
		return None
	else:
		return json.loads(r.text)["token"]

if(len(sys.argv) == 1):
	print("echo user required")
else:
	t = gettoken(sys.argv[1],len(sys.argv) > 2 and sys.argv[2] or getpass.getpass())
	if t:
		print("export \"PELARSTOKEN=%s\"" % t)
	else:
		print("echo Not valid")