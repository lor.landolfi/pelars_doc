function r = cell2matrix(a)

n = length(a{1});
r = zeros(n,length(a));
for I=1:length(a)
    for J=1:n
        r(J,I) = a{I}{J};
    end
end
