var page = require('webpage').create();
page.onConsoleMessage = function(msg)
{
    console.log("remote> " + msg);
};
console.log("before")
function waitFor(testFx, onReady, timeOutMillis,stepMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000 //< Default Max Timout is 3s
    if(!stepMillis)
    	stepMillis = 250
    start = new Date().getTime(),
    condition = false,
    interval = setInterval(function() {
        if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
            // If not time-out yet and condition not yet fulfilled
            condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
        } else {
            if(!condition) {
                // If condition still not fulfilled (timeout but condition is 'false')
                console.log("'waitFor()' timeout");
                phantom.exit(1);
            } else {
                // Condition fulfilled (timeout and/or condition is 'true')
                console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                clearInterval(interval); //< Stop this interval
            }
        }
    }, stepMillis); //< repeat check every 250ms
};

baseurl="http://pelars.sssup.it/pelarsModules/dataCard/wholeCard.html"
baseurl="http://pelars.sssup.it/dataCard/wholeCard.html"
baseurl="http://pelars.sssup.it/examples/gabcn_democard/"
baseurl="http://127.0.0.1:8000/dataCard/wholeCard.html"


page.open(baseurl + "?session=832", function() {
		counter1 = 0
	waitFor(function() 
	{
		console.log("try")
		return counter1++ > 10 || page.evaluate(function() { 
				var n = $("svg").children().length
				console.log("step ",n)
				return n
		})
	},
	function()
	{
		// way to wait
		counter = 0
		waitFor(function() {
			return counter++ > 5
		},
		function()
		{
		  page.render('datacard.png');
		  phantom.exit();

		})
	},
	10000,
	1000
	)
});