faces_vis = function() {

	var create = function(parent, sessionid, service)
	{
		var startTime,endTime



		var maxFaces;
		var faceUseComp = [];
		var faceNum = [];
		var faceRadius = 5;
		var maxRadius = faceRadius*4;
		var	faceTotal = [];
		var w = 800-55
		var  iconW = 15
		var h = 150
		var timeX ;
        var xAxis = d3.svg.axis();
        var xAxisScale;

		var faceSVGH = h-60;


		function goFace(parent, incomingData){
			var faceData = incomingData;

		
			var faceSVG = d3.select(parent) //timeline div id
				.append("svg")
				.attr("width",w)
				.attr("height",faceSVGH+5)  
				.style("margin-top","1px");

			var	facePic = faceSVG.append("g").attr("class","backlabels")
					.append("image")
				    .attr("x", 2)
				    .attr("y", faceSVGH/2-24+iconW)
				    .attr("width",iconW)
				    .attr("height",iconW)
			        .attr("xlink:href", "face2.png");
			var	labelsFace = faceSVG.append("g").attr("class","backlabels")
					.append("text")
				    .attr("x", 0)
				    .attr("text-anchor","left")
				    .attr("y", faceSVGH/2+iconW) //197
				    .text("Faces")
				    .attr("font-size",8);

			var faceAxisCall = faceSVG.append('g');
		    xAxisScale.domain([startTime, endTime]);

			    faceAxisCall.call(xAxis)
			        .attr("class", "axis") //Assign "axis" class
			            .attr("text-anchor", "end")
			        .attr('transform', 'translate(' + (4) + ', ' + (faceSVGH/2+100) + ')');
			var faceColor = "#AB47BC";
			var faceY = d3.scale.linear()
				.range([0,faceSVGH/4])
			rectFace = faceSVG.append("g").attr("class","facerect").selectAll(".facerect")
			    .data(faceData)
			  	.enter().append("rect")
			    .attr("class", "facerect")
			    .attr("x", function(d){
			    	faceNum.push(d.num);
			    	var maxFaces = d3.max(faceNum);
			    	faceY.domain([1, maxFaces])
			    	return timeX(d.time);
			    })
			    .attr("y", function(d,i){
			    	return faceSVGH/2-faceY(d.num)
			    })
			    .attr("height", function(d,i){
			    	if(2*faceY(d.num)<0){
			    		return 0;
			    	}else{
				    	return 2*faceY(d.num);
				    }
			    })
			    .attr("width",2)
			    .attr("fill", faceColor)
			    .attr("opacity",.6)
				.attr("stroke","none")
		}

//		service.getSession(sessionid, function ()
//		{
		service.getData(sessionid, "face",function (json)
		{
			startTime = json[0].time;
			endTime = json[json.length-1].time;
			timeX	= d3.scale.linear()
			timeX.domain([startTime, endTime]).range([14, w-50]);
			xAxisScale =  d3.time.scale()
            .domain([startTime, endTime])
            .range([10, w - 40]);

				goFace(parent,json);
		})

	}
    return {
    	create: create
    };
}()
