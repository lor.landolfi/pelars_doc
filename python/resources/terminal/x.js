var page = require('webpage').create();
page.onConsoleMessage = function(msg)
{
    console.log("remote> " + msg);
};
function waitFor(testFx, onReady, timeOutMillis,stepMillis) 
{
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000 //< Default Max Timout is 3s
    if(!stepMillis)
    	stepMillis = 250
    start = new Date().getTime(),
    condition = false,
    interval = setInterval(function() {
        if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
            // If not time-out yet and condition not yet fulfilled
            condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
        } else {
            if(!condition) {
                // If condition still not fulfilled (timeout but condition is 'false')
                console.log("'waitFor()' timeout");
                phantom.exit(1);
            } else {
                // Condition fulfilled (timeout and/or condition is 'true')
                console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                clearInterval(interval); //< Stop this interval
            }
        }
    }, stepMillis); //< repeat check every 250ms
};

console.log("before")
baseurl="http://127.0.0.1:8000/dataCard/wholeCard.html"

console.log("Acquiring from " + baseurl)
page.open(baseurl + "?session=832", function() {
	console.log("opened")
})
