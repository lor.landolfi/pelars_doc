/**
 * Easy PELARS Interface - Emanuele Ruffaldi 2016
 *
 * TODO: verify need of authentication (e.g. cookie if in same of baseurl)\
 */
	 var token = ""


	 function pelars_base()
	 {
		var baseurl = sessionStorage.getItem("baseurl")
					console.log("baseurl is",""+baseurl)
					if(!baseurl) baseurl = "http://pelars.sssup.it/pelars"
					return baseurl
	 }
	 var usetoken = true;

		function pelars_authenticate(cb)
		{
		 	console.log("pelars_authenticate",token,sessionStorage.getItem("token"))
			var jsres;
			var res = "";
			console.log("Location is",window.location.hostname )
			console.log("Token cookie is",document.cookie.indexOf("token"))
			console.log("Get token cookie",xgetCookie("token"))
			if(window.location.hostname == "pelars.sssup.it")
		 	{
		 		if(document.cookie.indexOf("token") >= 0)
		 		{
		 			token = xgetCookie("token")
		 			cb(true)
		 			return
		 			
		 			// TODO verify validity
		 			// show popup
		 			/*
		 			<form action="password" method="post" id="sub_form">
						<fieldset>
							<legend>Pelars Web Interface</legend>
							<p>
								Email: <input type="email" name="user">
							</p>

							<p>
								Password: <input type="password" name="pwd">
							</p>

							<input type="hidden" name="p-url" value=null> <input
								type="submit" value="Login">
						</fieldset>
					</form>
					*/
		 		}
		 	}			
			/*
			if (document.cookie.indexOf("token") >= 0) {
			  // They've been here before.
			  alert("hello again");
			  cb(true)
			}
			*/
			if(true)
			{
				if((""+sessionStorage.getItem("token")) != "null")
				{
					console.log("Existing Token1:"+sessionStorage.getItem("token"))
					cb(true)
					return
				}
				if(token)
				{
					console.log("Existing Token2")
					cb(true);
					return
				}
			var email = prompt("Please enter your email", "");
			var pswd = prompt("Please enter your password", "");

			var baseurl = pelars_base()
			jQuery.ajax({
				timeout : 1000,
				type : "POST",

				url : baseurl + "/password?user=" + email + "&pwd=" + pswd,
				async: false,
				success : function(jqXHR, status, result){
					console.log("SUCCESS",jqXHR)
					//jsres = JSON.parse(jqXHR);
					jsres = jqXHR;
					res = jsres["token"];
					sessionStorage.setItem("token",res)
					token = res
					console.log("SUCCESS:" + sessionStorage.getItem("token"))
					cb(true)
				},
				error : function(jqXHR, status) {
					res = 0; 
					cb(false)
				}
			});
			}
			cb(true)
			//document.getElementById("tag").innerHTML = res;
			return res;
		}

		function xgetCookie(cname) {
		    var name = cname + "=";
		    var ca = document.cookie.split(';');
		    for(var i=0; i<ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1);
		        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		    }
		    return "";
		}

		 function auth()
		 {
		 	if(window.location.hostname == "pelars.sssup.it")
		 	{
		 		if(document.cookie.indexOf("token") >= 0)
		 		{
		 			return xgetCookie("token")
		 			
		 			// TODO verify validity
		 			// show popup
		 			/*
		 			<form action="password" method="post" id="sub_form">
						<fieldset>
							<legend>Pelars Web Interface</legend>
							<p>
								Email: <input type="email" name="user">
							</p>

							<p>
								Password: <input type="password" name="pwd">
							</p>

							<input type="hidden" name="p-url" value=null> <input
								type="submit" value="Login">
						</fieldset>
					</form>
					*/
		 		}
		 	}
		 	var tt = sessionStorage.getItem("token")
		 	console.log("Auth check",token,tt)
				if(!token)
				{	
					if(tt)
						return tt
					else
					{
						token = prompt("Enter Token")
						if(token)
						sessionStorage.setItem("token",token)
						return token;
					}
				}	 
				else
				return token;	
		 }

		function pelarsajax(url,data,cb,mode)
		{
			var mode = mode ? mode : "GET"
			var token = auth()
			console.log("done auth:"+token)
			var baseurl = pelars_base()
			url = baseurl + url
			if(usetoken)
			{
				console.log("added token:"+url)
				url = url + "?token="+token
			}
			$('#loading').html("loading " + url);
			var data;
			// create a data set with groups
			jQuery.ajax({
				dataType : "json",
				url : url,
				data : data,
//								beforeSend: function(xhr){xhr.setRequestHeader('Origin', "http://127.0.0.1:8000");},

				type: mode,
				crossDomain: true,
				//xhrFields: {
			     // withCredentials: true
				//},
				success : function(data) {
					$('#loading').html("");
					cb(data)
				},
				error : function(xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
					$('#loading').html("");
				}
			});			
		} 

		function getSession(session_id,cb) {

			// DOM element where the Timeline will be attached
			pelarsajax("/session/" + session_id,undefined, function (data)
			{
				cb(data,session_id)
			})
			return true;
		}

		function getSessions(cb) {

			// DOM element where the Timeline will be attached
			pelarsajax("/session/" + session_id,undefined,cb);
			return true;
		}

		function getOpResult(session_id,name,cb) {

			pelarsajax("/content/" + session_id + "/" +name ,undefined, function (json)
			{
				try
				{
					cb(JSON.parse(json[0]["result"]));
				}
				catch(e)
				{
					cb(json[0]["result"])
				}
			})
		}

		function getData(session_id,type,cb) {
			if(type)
				type = "/"+type
			// DOM element where the Timeline will be attached
			pelarsajax("/data/" + session_id + type ,undefined, function (json)
			{
				console.log("Result for " + session_id + " on type " + type + " " + json.length)
				cb(json,session_id)
			})
			return true;
		}	

		function getPhases(session_id,cb) {

			// DOM element where the Timeline will be attached
			pelarsajax("/phase/" + session_id,undefined, function (data)
			{
				cb(data,session_id)
			})
			return true;
		}	

		pelars ={
			getSession: getSession,
			getData : getData,
			getPhases : getPhases,
			getOpResult: getOpResult,
			authenticate: pelars_authenticate
		}

