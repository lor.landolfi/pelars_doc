import numpy,math
import yaml,json
import cv2
import * from common_math

# Calibration: /calibration
# calibration_webcam.xml
class Calibration:
    def __init__(self,d):
        self.session = d["session"]
        self.type = d["type"]
        #self.width = d["width"]
        #self.height = d["height"]
        c = np.array(d["parameters"])
        self.rot = np.reshape(c[0:9],(3,3))
        self.pos = np.reshape(c[9:],(3,1))
    def __repr__(self):
        return "Calibration(%s,%s,%s)" % (self.type,self.pos,self.rot)

# Calibration: /calibration
# calibration_webcam.xml
class Intrinsics:
    def __init__(self,d):
        if type(d) is dict:
            self.session = d["session"]
            self.type = d["type"]
            #self.width = d["width"]
            #self.height = d["height"]
            c = np.array(d["parameters"])
            self.rot = np.reshape(c[0:9],(3,3))
            self.pos = np.reshape(c[9:],(3,1))
        elif d.endswith("yml") or d.endswith("yaml"):
            print "loading intrinsics",d
            y = yaml.load(open(d,"rb"))
            # rgb_intrinsics
            # rgb_distortion
            self.K = np.reshape(np.array(y["rgb_intrinsics"]["data"]),(3,3)) 
            self.d = np.array(y["rgb_distortion"]["data"])
            self.width = y["width"]
            self.height = y["height"]

    def __repr__(self):
        return "Intrinsics()"
    def flipxpoint2(self,p):
        return (self.width-p[0],p[1])
    def flipxpoints2(self,pp):
        return [self.flipxpoint2(p) for p in pp]
    def makepose4points(self,op,ip):
        N = op.shape[0]
        print op.shape,ip.shape,N
        retval,rvec,tvec = cv2.solvePnP(np.reshape(op,(N,1,3)),np.reshape(ip,(N,1,2)),self.K,None)#self.d)
        rmtx,J = cv2.Rodrigues(rvec)
        r = np.identity(4)
        r[0:3,0:3] = rmtx
        r[0,3] = tvec[0]
        r[1,3] = tvec[1]
        r[2,3] = tvec[2]
        return r
    def flipxpoint3(self,p):
        Z = p[2]
        w = self.width
        cx = self.K[0,2]
        fx = self.K[0,0]
        return (-(2*Z*cx - w + p[0]*fx)/fx,p[1],Z)
    def project(self,pnt):
        Z = pnt[2]
        fx = self.K[0,0]
        fy = self.K[1,1]
        cx = self.K[0,2]
        cy = self.K[1,2]
        return (pnt[0]*fx/Z+cx,pnt[1]*fy/Z+cy)
    def flipinvpose(self,pose,pp):
        return numpy.linalg.inv(self.flippose(numpy.linalg.inv(pose),pp))

    def flippose(self,pose,pp):
        """Take a pose, extract plane of 4 points"""
        pose = np.reshape(np.array(pose),(4,4))

        #pp = [(-1,0,-1,1),(-1,0,1,1),(1,0,1,1),(1,0,-1,1)]
        #return makepose_from_3_points([self.flipxpoint3(p) for p in pp])
        ip = np.array([self.flipxpoint2(self.project(np.dot(pose,x))) for x in pp])
        op = np.array([p[0:3] for p in pp]).astype(np.float64)
        print "ip",ip
        print "op",op
        return self.makepose4points(op,ip)
    def regenpose(self,pose):
        """it works"""
        pose = np.reshape(np.array(pose),(4,4))
        # following is same as np.dot(pose,origin .. x ... y)
        pp = [pose[0:3,3],pose[0:3,3]+pose[0:3,0],pose[0:3,3]+pose[0:3,1]]
        ppo = [p for p in pp]
        return makepose_from_3_points(ppo)