#https://realpython.com/blog/python/face-detection-in-python-using-a-webcam/
import cv2
import sys
import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--step",default=0,type=int,help="pause")
    parser.add_argument("video")

    args = parser.parse_args()

    #cvHaarDetectObjects
    #"haarcascade_frontalface_default.xml",
    allpaths = ["haarcascade_frontalface_alt.xml","haarcascade_profileface.xml"]

    bp = "/opt/local/share/OpenCV/haarcascades/"
    fcs = []
    for x in allpaths:
        fc = cv2.CascadeClassifier(os.path.join(bp,x))
        fcs.append(fc)
    print "loaded",len(fcs)
    eye_cascade  = cv2.CascadeClassifier(os.path.join(bp,'haarcascade_eye_tree_eyeglasses.xml'))

    if args.video == "0":
        video_capture = cv2.VideoCapture(0)
    else:
        video_capture = cv2.VideoCapture(args.video)
    surf = cv2.SURF(400)
    knn = cv2.KNearest()

    # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)   # or pass empty dictionary

    #flann = cv2.FlannBasedMatcher(index_params,search_params)
    #matches = flann.knnMatch(des1,des2,k=2)
    #matchesMask = [[0,0] for i in xrange(len(matches))]
    #http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html
    #
    #retval, results, neigh_resp, dists = knn.find_nearest(des,1)
    #templateg= cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)
    #keys,desc = surf.detect(templateg,None,useProvidedKeypoints = False)
    fid = 0
    while True:
        fid += 1
        # Capture frame-by-frame
        ret, frame = video_capture.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        k = 0
        ss = set()
        for j,fc in enumerate(fcs):
            faces = fc.detectMultiScale(gray, 1.3, 5)
            for (x,y,w,h) in faces:
                ss.add((x,y,w,h))
        print fid,len(ss)
        for (x,y,w,h) in ss:
            print "\t",fid,j,(x,y,w,h)
            if args.step >= 0:
                cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
            k += 1
            cv2.imwrite("face_%d_%d.png" % (fid,k),frame[y:y+h,x:x+w,:])
            continue
            kp, des = surf.detectAndCompute(frame[y:y+h, x:x+w],None)

            roi_gray = gray[y:y+h, x:x+w]
            roi_color = frame[y:y+h, x:x+w]
            eyes = eye_cascade.detectMultiScale(roi_gray,1.1,2,0)
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
        # Display the resulting frame

        if args.step >= 0:
            cv2.imshow('Video', frame)
            if cv2.waitKey(args.step) & 0xFF == ord('q'):
                break

    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()
