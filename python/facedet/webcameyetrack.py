#Report: # https://docs.google.com/document/d/1KxsAlYX5aZUDzzxPj5VfSHkypMYnsU9jHgv4GHT8ARw/edit
#
#Original:
#   http://wiki.ros.org/pi_face_tracker
#  Good Features to Track and the Lucas-Kanade Optical Flow 
#  https://github.com/pirobot/rbx1/tree/indigo-devel/rbx1_vision/src/rbx1_vision
#
# Face Detection and Recognition
# TODO http://www.shervinemami.info/faceRecognition.html
# TUTORIAL http://docs.opencv.org/2.4/modules/contrib/doc/facerec/facerec_tutorial.html
#
# Eigenfaces (see createEigenFaceRecognizer())
# Fisherfaces (see createFisherFaceRecognizer())
# Local Binary Patterns Histograms (see createLBPHFaceRecognizer())
#
# real-time face recognition without database

import cv2
import sys
import argparse
import os
import numpy as np

def rect_extent2limit(tp):
    return (tp[0],tp[1],tp[0]+tp[2],tp[1]+tp[3])

def rect_limit2extent(tp):
    return (tp[0],tp[1],tp[2]-tp[0],tp[3]-tp[1])

def rect_pts2extent(pts):
    left = min([p[0] for p in pts])
    bottom = min([p[1] for p in pts])
    right = max([p[0] for p in pts])
    top = max([p[1] for p in pts])
    return rect_limit2extent((left,bottom,right,top,))


def rect_extent_intersect(tp1,tp2):
    p1 = rect_extent2limit(tp1)
    p2 = rect_extent2limit(tp2)

    left = max(p1[0],p2[0])
    bottom = min(p1[1],p1[1])
    right = min(p1[2],p2[2])
    top = max(p1[3],p1[3])
    return rect_limit2extent((left,bottom,right,top,))

def rect_extent_area(tp1):
    return tp1[2]*tp1[3]

class Face:
    index = 1
    def __init__(self,input_image,tp,gf_params,lk_params):
        x,y,w,h = tp
        self.index = Face.index
        Face.index += 1
        self.lk_params = lk_params
        #self.mask = np.zeros_like(input_image)
        #self.mask[y:y+h, x:x+w] = 255
        self.lk_params = dict()
        self.track_box = tp
        keypoints = []
        kp = cv2.goodFeaturesToTrack(input_image[y:y+h, x:x+w], **gf_params)
        if kp is not None and len(kp) > 0:
            for cx, cy in np.float32(kp).reshape(-1, 2):
                keypoints.append((x+cx, y+cy))
        self.keypoints = keypoints
    def area(self):
        if self.track_box is None:
            return 0
        else:
            return rect_extent_area(self.track_box)
    def track(self,input_image,prev_image):
        img0, img1 = previousimage, input_image
        p0 = np.float32([p for p in self.keypoints]).reshape(-1, 1, 2)
        p1, st, err = cv2.calcOpticalFlowPyrLK(img0, img1, p0, None, **self.lk_params)

        try:
            p0r, st, err = cv2.calcOpticalFlowPyrLK(img1, img0, p1, None, **self.lk_params)
            
            # Compute the distance between corresponding points in the two flows
            d = abs(p0-p0r).reshape(-1, 2).max(-1)
            
            # If the distance between pairs of points is < 1 pixel, set
            # a value in the "good" array to True, otherwise False
            good = d < 1
        
            # Initialize a list to hold new keypoints
            new_keypoints = list()
            
            # Cycle through all current and new keypoints and only keep
            # those that satisfy the "good" condition above
            for (x, y), good_flag in zip(p1.reshape(-1, 2), good):
                if not good_flag:
                    continue
                new_keypoints.append((x, y))
                
                # Draw the keypoint on the image
                #cv2.circle(self.marker_image, (x, y), self.feature_size, (0, 255, 0, 0), cv.CV_FILLED, 8, 0)
            
            # Set the global keypoint list to the new list    
            self.keypoints = new_keypoints
            
            # Convert the keypoints list to a numpy array
            keypoints_array = np.float32([p for p in self.keypoints]).reshape(-1, 1, 2)  
            
            # If we have enough points, find the best fit ellipse around them
            if len(self.keypoints) > 6:
                ell = cv2.fitEllipse(keypoints_array)
                ellpts = cv2.boxPoints(ell)
                track_box = rect_pts2extent(ellpts)
                #print "track_box","fit",track_box,"from",ell,"as",ellpts
            else:
                # Otherwise, find the best fitting rectangle
                track_box = cv2.boundingRect(keypoints_array)
                #print "track_box","bb",track_box
        except:
            track_box = None
        self.track_box = track_box

def xmin(w,default):
    if len(w) == 0:
        return default
    else:
        return min(w)
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--step",default=0,type=int,help="pause")
    parser.add_argument("video")

    args = parser.parse_args()

    #cvHaarDetectObjects
    #"haarcascade_frontalface_default.xml",
    allpaths = ["haarcascade_frontalface_alt.xml","haarcascade_profileface.xml"]

    bp = "/opt/local/share/OpenCV/haarcascades/"
    fcs = []
    for x in allpaths:
        fc = cv2.CascadeClassifier(os.path.join(bp,x))
        fcs.append(fc)
    print "loaded",len(fcs)
    eye_cascade  = cv2.CascadeClassifier(os.path.join(bp,'haarcascade_eye_tree_eyeglasses.xml'))

    if args.video == "0":
        video_capture = cv2.VideoCapture(0)
    else:
        video_capture = cv2.VideoCapture(args.video)
    #surf = cv2.SURF(400)
    #knn = cv2.KNearest()

    haar_params = dict(scaleFactor = 1.3,
                                minNeighbors = 3,
                                flags = 0,#cv2.HAAR_DO_CANNY_PRUNING,
                                minSize = (20,20),
                                maxSize = (150,150))

    gf_params = dict(maxCorners = 200, 
                           qualityLevel = 0.02,
                           minDistance = 7,
                           blockSize = 10,
                           useHarrisDetector = True,
                           k = 0.04)

    lk_params = dict( winSize  = (10,10), 
                  maxLevel = 2, 
                  criteria =(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 20, 0.01))   
    # FLANN parameters
    #FLANN_INDEX_KDTREE = 0
    #index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    #search_params = dict(checks=50)   # or pass empty dictionary

    #flann = cv2.FlannBasedMatcher(index_params,search_params)
    #matches = flann.knnMatch(des1,des2,k=2)
    #matchesMask = [[0,0] for i in xrange(len(matches))]
    #http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html
    #
    #retval, results, neigh_resp, dists = knn.find_nearest(des,1)
    #templateg= cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)
    #keys,desc = surf.detect(templateg,None,useProvidedKeypoints = False)
    fid = 0
    allfaces = []
    previousimage = None
    while True:
        fid += 1
        # Capture frame-by-frame
        ret, frame = video_capture.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        k = 0
        ss = set()
        for j,fc in enumerate(fcs):
            faces = fc.detectMultiScale(gray, **haar_params)
            for (x,y,w,h) in faces:
                # TODO: remove the similar ones
                ss.add((x,y,w,h))

        if previousimage is not None and len(allfaces) > 0:
            n = len(allfaces)
            for x in allfaces:
                x.track(gray,previousimage)

            allfaces = [x for x in allfaces if x.area() > 50]
            n2 = len(allfaces)
            #print "tracking",n,"->",n2

        # then remove the ss that are not too near
        if len(allfaces) == 0:
            gss = ss
        else:
            gss = set()        
            for s in ss:
                minarea = xmin([rect_extent_area(rect_extent_intersect(s,x.track_box)) for x in allfaces if x.track_box is not None],default=100000)
                #print "minarea frame",fid,s,minarea
                if minarea > 300:
                    gss.add(s)

        for x in gss:
            #print fid,"\tnew",x
            allfaces.append(Face(gray,x,gf_params,lk_params))

        previousimage = gray

        for f in allfaces:
            print fid,f.index,f.track_box
            
        if args.step >= 0:
            for p in allfaces:
                x,y,w,h = p.track_box
                cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
                cv2.putText(frame,str(p.index),(x+w/2,y+h/2),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255))
            cv2.imshow('Video', frame)

            if cv2.waitKey(args.step) & 0xFF == ord('q'):
                break

    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()
