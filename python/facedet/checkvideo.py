import os,sys,argparse,json
from subprocess import check_output

COLORG = '\x1b[6;30;42m' 
COLOR0 = '\x1b[0m'

def getvideoinfo(vm):
	fr = 0
	du = 0
	p = ["ffprobe","-v","error","-show_format","-show_streams",vm]
	#print " ".join(p)
	try:
		for line in check_output(p,stderr=None,shell=False).split("\n"):
			if line.startswith("duration="):
				x = line.split("=",1)[1]
				if x != "N/A":
					du = float(x)
				else:
					du = -2
			elif line.startswith("nb_frames="):
				x = line.split("=",1)[1]
				if x != "N/A":
					fr = int(x)
				else:
					fr = -2
	except:
		print sys.exc_info()
		fr = -1
		du = -1
	return (fr,du)
def main():

	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('--good',action="store_true")
	parser.add_argument('--ucv',action="store_true")
	args = parser.parse_args()
	allsessions = []
	p = os.environ.get("PELARSDATA","/Volumes/BigData/PELARS/sessions")
	if not args.good and not args.ucv:
		print "ALL downloaded"
		for s in os.listdir(p):
			fp = os.path.join(p,s)
			if s[0] == "S" and os.path.isdir(fp):
				allsessions.append(int(s[1:]))
	elif not args.ucv:
		print "GOOD only"
		for s in json.load(open(os.path.join(p,"goodsessions.json"),"rb")):
			allsessions.append(s["session"])
	else:
		print "UCV"
		for q in open(os.path.join(p,"session_scores.csv"),"rb"):
			a = q.split("\t")
			if a[0] != "SessionID":
				allsessions.append(int(a[0]))

	for s in allsessions:
		fp = os.path.join(p,"S%d" % s)
		if os.path.isdir(fp):
			km = os.path.join(fp,"kinect.mp4")
			wm = os.path.join(fp,"webcam.mp4")
			mm = fp + "_multimedias.json"
			kframes,kduration = (0,0)
			wframes,wduration = (0,0)
			if os.path.isfile(km):
				kframes,kduration = getvideoinfo(km)
			if os.path.isfile(wm):
				wframes,wduration = getvideoinfo(wm)
			first = dict(screen=0,people=0,workspace=0)
			last = dict(screen=0,people=0,workspace=0)
			count = dict(screen=0,people=0,workspace=0)
			durationmedia = dict(screen=0,people=0,workspace=0)
			if os.path.isfile(mm):
				w = json.load(open(mm,"rb"))

				# first and last
				for x in w:
					if "view" in x and x.get("trigger","") == "automatic":
						y = first.get(x["view"])
						if y is 0:
							first[x["view"]] = x["time"]
						last[x["view"]] = x["time"]
						count[x["view"]] += 1
				for k in last.keys():
					durationmedia[k] = (last[k]-first[k])/1000.0
			print "session",s,"kinect",COLORG if kframes <= 0 else "", (kframes,kduration),COLOR0,"webcam",COLORG if wframes <= 0 else "",(wframes,wduration),COLOR0,"first",first,"last",last,"count",count,"duration",durationmedia

if __name__ == '__main__':
	main()