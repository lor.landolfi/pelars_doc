%%
mysetup('jsonmex');
mysetup('exportfig');
%%
allfonts = @(x) set(findall(gcf,'-property','FontSize'),'FontSize',x)
%%
%sid = 542;
sid = 615;
%%
a = fromjson(fileread(sprintf('S%d_handspos.json',sid)));
a = cell2matrix(a)';
%axisonames = {'x (m)','y (m)','z (m)'};
axisids = [1,2,3];
axisnames = {'y (m)','x (m)','z (m)'};
axisnames = {'y (m)','x (m)','z (m)'};
ds = dataset([a,{'id','x','y','z'}]);
%ds.z = ds.z*1000;
ds.pos = [ds.x,ds.y,ds.z];

summary(ds)

%%
q1=ds;
q1.vx = q1.pos(:,axisids(1));
q1.vy = q1.pos(:,axisids(2));
q1.vz = q1.pos(:,axisids(3));
ids = unique(q1.id);
colors = 'rgbmkg';
for I=1:length(ids)
    q2 = q1(q1.id ==ids(I),:);
    plot3(q2.vx,q2.vy,q2.vz,colors(I),'LineStyle',':');
    scatter3(q2.vx,q2.vy,q2.vz,0.5,colors(I),'filled');
    hold on
end
hold off
axis equal
xlabel(axisnames{1});
ylabel(axisnames{2});
zlabel(axisnames{3});
title(sprintf('Session %d - Motion Paths colored by hand identifier',sid));
allfonts(20)

set(gcf,'Position',[1           5        1440         800]);
export_fig(sprintf('S%d_hands',sid),'-pdf');
%%
hids = histc(q1.id,ids);
[m,mi] = max(hids);
q2 = q1(q1.id ==ids(mi),:);
plot3(q2.vx,q2.vy,q2.vz,'k','LineStyle',':');
hold on
scatter3(q2.vx,q2.vy,q2.vz,[],1:length(q2),'filled');
hold off
axis equal
colorbar
xlabel(axisnames{1});
ylabel(axisnames{2});
zlabel(axisnames{3});
grid on
title(sprintf('Session %d - Longest Path colored by time',sid));
allfonts(20)
set(gcf,'Position',[1           5        1440         800]);

export_fig(sprintf('S%d_handtime',sid),'-pdf');

%%
%http://stackoverflow.com/questions/31063591/in-matlab-how-to-smooth-pixels-in-2d-heatmap-using-imagesc
xy = double([ds.x,ds.y]);
h = hist3(xy,[20,20]);
hist3(xy,[50,50]);
heatmap(h)
xlabel('x');
ylabel('y');
title(sprintf('Session %d - Heat Map of coordinates',sid));
allfonts(20)
set(gcf,'Position',[1           5        1440         800]);

export_fig(sprintf('S%d_handmap',sid),'-png');

